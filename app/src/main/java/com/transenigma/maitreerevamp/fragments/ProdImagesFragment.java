package com.transenigma.maitreerevamp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProdImagesFragment extends Fragment {

    private ImageView productImage;

    public static ProdImagesFragment newInstance(String productImgUrl) {
        ProdImagesFragment myFragment = new ProdImagesFragment();

        Bundle args = new Bundle();
        args.putString("productImgUrl", productImgUrl);
        myFragment.setArguments(args);

        return myFragment;
    }

    public ProdImagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_prod_images, container, false);

        productImage = (ImageView) root_view.findViewById(R.id.product_details_img);

        String strtext = getArguments().getString("productImgUrl");

        if (strtext != null) {
            Picasso.with(getContext()).load(strtext).into(productImage);
        }

        return root_view;
    }
}
