package com.transenigma.maitreerevamp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProdSpecFragment extends Fragment {

    private TextView prodSpecHeader;
    private TableLayout prodSpecTabLayout;

    public static ProdSpecFragment newInstance(String productSpec) {
        ProdSpecFragment myFragment = new ProdSpecFragment();

        Bundle args = new Bundle();
        args.putString("productSpec", productSpec);
        myFragment.setArguments(args);

        return myFragment;
    }

    public ProdSpecFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_prod_spec, container, false);

        prodSpecTabLayout = (TableLayout) root_view.findViewById(R.id.product_spec_tableLayout);
        prodSpecHeader = (TextView) root_view.findViewById(R.id.textview_spec_header);

        String strText = getArguments().getString("productSpec");

        Log.i(TAG, "onCreateView: "+strText);

        JSONObject prodSpecObj = null;
        try {
            prodSpecObj = new JSONObject(strText);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        TableRow.LayoutParams trlp = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, 0.33f);

        Iterator<String> iter = prodSpecObj.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                Object value = prodSpecObj.get(key);

                TableRow tbrow = new TableRow(getContext());
                TextView t1v = new TextView(getContext());
                t1v.setText(key);
                t1v.setGravity(Gravity.LEFT);
                t1v.setLayoutParams(trlp);
                t1v.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
                tbrow.addView(t1v);
                TextView t2v = new TextView(getContext());
                t2v.setText(value.toString());
                t2v.setGravity(Gravity.LEFT);
                t2v.setLayoutParams(trlp);
                t2v.setTextColor(ResourcesCompat.getColor(getResources(), R.color.black, null));
                tbrow.addView(t2v);
                prodSpecTabLayout.addView(tbrow);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return root_view;
    }
}