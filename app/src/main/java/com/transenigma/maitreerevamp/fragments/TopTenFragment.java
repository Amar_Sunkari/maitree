package com.transenigma.maitreerevamp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.LeaderBoardAdapter;
import com.transenigma.maitreerevamp.models.DealerDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 */
public class TopTenFragment extends Fragment {

//    WebView prodDetailsWebView;

    private RecyclerView topTenRecyclerView;

    private RecyclerView.LayoutManager topTenLayoutManager;

    private LeaderBoardAdapter leader_board_Adapter;
    private final ArrayList<DealerDetails> dealers = new ArrayList<>();

    public static TopTenFragment newInstance(String params) {
        TopTenFragment myFragment = new TopTenFragment();

        Bundle args = new Bundle();
        args.putString("params", params);
        myFragment.setArguments(args);

        return myFragment;
    }

    public TopTenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_top_ten, container, false);

        topTenRecyclerView = (RecyclerView) root_view.findViewById(R.id.top_ten_recyclerView);
        topTenLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        topTenRecyclerView.setLayoutManager(topTenLayoutManager);

        try {
            JSONObject topTenObj = new JSONObject(getArguments().getString("params"));

            if (topTenObj.length() > 0) {
                dealers.add(new DealerDetails(topTenObj.getString("1"), "", "", "1"));
                dealers.add(new DealerDetails(topTenObj.getString("2"), "", "", "2"));
                dealers.add(new DealerDetails(topTenObj.getString("3"), "", "", "3"));
                dealers.add(new DealerDetails(topTenObj.getString("4"), "", "", "4"));
                dealers.add(new DealerDetails(topTenObj.getString("5"), "", "", "5"));
                dealers.add(new DealerDetails(topTenObj.getString("6"), "", "", "6"));
                dealers.add(new DealerDetails(topTenObj.getString("7"), "", "", "7"));
                dealers.add(new DealerDetails(topTenObj.getString("8"), "", "", "8"));
                dealers.add(new DealerDetails(topTenObj.getString("9"), "", "", "9"));
                dealers.add(new DealerDetails(topTenObj.getString("10"), "", "", "10"));
            }
        } catch (JSONException e) {
//            e.printStackTrace();

            Log.i(TAG, "onCreateView: Something wrong!");
        }

        leader_board_Adapter = new LeaderBoardAdapter(dealers, getActivity().getBaseContext());
        topTenRecyclerView.setAdapter(leader_board_Adapter);

        return root_view;
    }
}
