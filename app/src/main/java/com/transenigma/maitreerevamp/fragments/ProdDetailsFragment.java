package com.transenigma.maitreerevamp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.transenigma.maitreerevamp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProdDetailsFragment extends Fragment {

    private WebView prodDetailsWebView;

    public static ProdDetailsFragment newInstance(String params) {
        ProdDetailsFragment myFragment = new ProdDetailsFragment();

        Bundle args = new Bundle();
        args.putString("params", params);
        myFragment.setArguments(args);

        return myFragment;
    }

    public ProdDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_prod_details, container, false);

        prodDetailsWebView = (WebView) root_view.findViewById(R.id.product_details_detail_prodDetailsWebView);

        prodDetailsWebView.getSettings().getJavaScriptEnabled();
        prodDetailsWebView.setWebViewClient(new WebViewClient());
        prodDetailsWebView.setBackgroundColor(Color.TRANSPARENT);
        prodDetailsWebView.setLayerType(prodDetailsWebView.LAYER_TYPE_SOFTWARE, null);

        String strtext = getArguments().getString("params");
        if (strtext == null || strtext.isEmpty()) {
            strtext = "<div style='color:#000000; text-align:center;'>\n" +
                    "<p>No description found! </p>\n" +
                    "</div>";
        } else {
            strtext = "<div style='color:#000000; text-align:center;'>\n" + strtext +
                    "</div>";
        }

        prodDetailsWebView.loadDataWithBaseURL(null, strtext, "text/html; charset=utf-8", "UTF-8", null);

        return root_view;
    }
}
