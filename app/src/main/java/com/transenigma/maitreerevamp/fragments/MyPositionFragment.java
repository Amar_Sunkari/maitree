package com.transenigma.maitreerevamp.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.LeaderBoardAdapter;
import com.transenigma.maitreerevamp.models.DealerDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyPositionFragment extends Fragment {

//    WebView prodDetailsWebView;

    private RecyclerView myPositionRecyclerView;
    private RecyclerView.LayoutManager myPositionLayoutManager;

    private LeaderBoardAdapter leader_board_Adapter;
    private final ArrayList<DealerDetails> dealers = new ArrayList<>();

    public static MyPositionFragment newInstance(String params) {
        MyPositionFragment myFragment = new MyPositionFragment();

        Bundle args = new Bundle();
        args.putString("params", params);
        myFragment.setArguments(args);

        return myFragment;
    }

    public MyPositionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root_view = inflater.inflate(R.layout.fragment_my_position, container, false);

        myPositionRecyclerView = (RecyclerView) root_view.findViewById(R.id.my_position_recyclerView);
        myPositionLayoutManager = new LinearLayoutManager(getActivity().getBaseContext());

        myPositionRecyclerView.setLayoutManager(myPositionLayoutManager);

        try {
            JSONObject topTenObj = new JSONObject(getArguments().getString("params"));

            if (topTenObj.length() > 0) {
                dealers.add(new DealerDetails(topTenObj.getString("1"), "", "", "1"));
                dealers.add(new DealerDetails(topTenObj.getString("2"), "", "", "2"));
                dealers.add(new DealerDetails(topTenObj.getString("3"), "", "", "3"));
                dealers.add(new DealerDetails(topTenObj.getString("4"), "", "", "4"));
                dealers.add(new DealerDetails(topTenObj.getString("5"), "", "", "5"));
                dealers.add(new DealerDetails(topTenObj.getString("6"), "", "", "6"));
                dealers.add(new DealerDetails(topTenObj.getString("7"), "", "", "7"));
                dealers.add(new DealerDetails(topTenObj.getString("8"), "", "", "8"));
                dealers.add(new DealerDetails(topTenObj.getString("9"), "", "", "9"));
                dealers.add(new DealerDetails(topTenObj.getString("10"), "", "", "10"));
            }
        } catch (JSONException e) {
//            e.printStackTrace();

            Log.i(TAG, "onCreateView: Something wrong!");
        }

        leader_board_Adapter = new LeaderBoardAdapter(dealers, getActivity().getBaseContext());
        myPositionRecyclerView.setAdapter(leader_board_Adapter);

        /*
        prodDetailsWebView = (WebView) root_view.findViewById(R.id.product_details_detail_prodDetailsWebView);

        prodDetailsWebView.getSettings().getJavaScriptEnabled();
        prodDetailsWebView.setWebViewClient(new WebViewClient());
        prodDetailsWebView.setBackgroundColor(Color.TRANSPARENT);
        prodDetailsWebView.setLayerType(prodDetailsWebView.LAYER_TYPE_SOFTWARE, null);

        String strtext = getArguments().getString("params");
        if (strtext == null || strtext.isEmpty()) {
            strtext = "<div style='color:#000000; text-align:center;'>\n" +
                    "<p>No description found! </p>\n" +
                    "</div>";
        } else {
            strtext = "<div style='color:#000000; text-align:center;'>\n" + strtext +
                    "</div>";
        }

        prodDetailsWebView.loadDataWithBaseURL(null, strtext, "text/html; charset=utf-8", "UTF-8", null);
*/


        return root_view;
    }
}
