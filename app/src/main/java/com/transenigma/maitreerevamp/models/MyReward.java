package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 6/11/16.
 */

public class MyReward {

    private String prod_img_url, prod_id, prod_name, prod_pts, prod_params, prod_details;

    public MyReward(String prod_img_url, String prod_id, String prod_name, String prod_pts,
                    String prod_params, String prod_details) {
        this.setProd_img_url(prod_img_url);
        this.setProd_id(prod_id);
        this.setProd_name(prod_name);
        this.setProd_pts(prod_pts);
        this.setProd_params(prod_params);
        this.setProd_details(prod_details);
    }

    public String getProd_details() {
        return prod_details;
    }

    private void setProd_details(String prod_details) {
        this.prod_details = prod_details;
    }

    public String getProd_img_url() {
        return prod_img_url;
    }

    private void setProd_img_url(String prod_img_url) {
        this.prod_img_url = prod_img_url;
    }

    public String getProd_id() {
        return prod_id;
    }

    private void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    private void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_pts() {
        return prod_pts;
    }

    private void setProd_pts(String prod_pts) {
        this.prod_pts = prod_pts;
    }

    public String getProd_params() {
        return prod_params;
    }

    private void setProd_params(String prod_params) {
        this.prod_params = prod_params;
    }
}
