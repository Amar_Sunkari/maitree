package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 13/4/17.
 */

public class ApprovedSaleCategory {

    private String cat_name;
    private String cat_id;

    public ApprovedSaleCategory(String cat_name, String cat_id) {
        this.cat_name = cat_name;
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    @Override
    public String toString() {
        return this.cat_name;
    }
}
