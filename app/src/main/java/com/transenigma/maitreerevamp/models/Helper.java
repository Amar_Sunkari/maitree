package com.transenigma.maitreerevamp.models;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.AboutProgramActivity;
import com.transenigma.maitreerevamp.activities.AboutUsActivity;
import com.transenigma.maitreerevamp.activities.ApproveSaleActivity;
import com.transenigma.maitreerevamp.activities.LeaderBoardActivity;
import com.transenigma.maitreerevamp.activities.MyOrdersActivity;
import com.transenigma.maitreerevamp.activities.MyProfileActivity;
import com.transenigma.maitreerevamp.activities.MyRewardsActivity;
import com.transenigma.maitreerevamp.activities.NewsActivity;

/**
 * Created by Amar on 05-Mar-17.
 */

public class Helper {

    private static int menuId = 0;

    public static ActionBarDrawerToggle setupActivity(final AppCompatActivity activity,
                                                      ActionBarDrawerToggle
                                                              actionBarDrawerToggle, final
                                                      DrawerLayout drawerLayout, Toolbar toolbar,
                                                      String title, NavigationView navigationView) {

        activity.setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(activity, drawerLayout, toolbar, R
                .string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {

                switch (menuId) {
                    /* case R.id.home_id:
                        if (!(activity instanceof DashActivity))
                            activity.startActivity(new Intent(activity, DashActivity.class));
                        break; */
                    case R.id.profile_id:
                        if (!(activity instanceof MyProfileActivity))
                            activity.startActivity(new Intent(activity, MyProfileActivity.class));
                        break;
                    case R.id.newsupdate_id:
                        if (!(activity instanceof NewsActivity))
                            activity.startActivity(new Intent(activity, NewsActivity.class));
                        break;
                    case R.id.rewards_id:
                        if (!(activity instanceof MyRewardsActivity))
                            activity.startActivity(new Intent(activity, MyRewardsActivity.class));
                        break;
                    case R.id.orders_id:
                        if (!(activity instanceof MyOrdersActivity))
                            activity.startActivity(new Intent(activity, MyOrdersActivity.class));
                        break;
                    case R.id.leaderboard_id:
                        if (!(activity instanceof LeaderBoardActivity))
                            activity.startActivity(new Intent(activity, LeaderBoardActivity.class));
                        break;
                    case R.id.aboutus_id:
                        if (!(activity instanceof AboutUsActivity))
                            activity.startActivity(new Intent(activity, AboutUsActivity.class));
                        break;
                    case R.id.about_program:
                        if (!(activity instanceof AboutProgramActivity))
                            activity.startActivity(new Intent(activity, AboutProgramActivity
                                    .class));
                        break;
                    case R.id.approve_sales:
                        if (!(activity instanceof AboutProgramActivity))
                            activity.startActivity(new Intent(activity, ApproveSaleActivity
                                    .class));
                        break;
                }
                menuId = 0;
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        activity.getSupportActionBar().setTitle(title);

        navigationView.setNavigationItemSelectedListener(new NavigationView
                .OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                menuId = item.getItemId();
                drawerLayout.closeDrawers();
                return false;
            }
        });

        return actionBarDrawerToggle;
    }
}