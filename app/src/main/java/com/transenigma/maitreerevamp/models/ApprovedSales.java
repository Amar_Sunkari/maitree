package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 12/4/17.
 */

public class ApprovedSales {

    private String title;
    private String id;
    private String prod_name;
    private String prod_qty;
    private String prod_unit;
    private String prod_regn_date;
    private String sale_id;
    private String contact_id;
    private Boolean isSelected;

    public ApprovedSales(String title, String id, String prod_name, String prod_qty, String prod_unit, String
            prod_regn_date, String sale_id, String contact_id) {
        this.title = title;
        this.id = id;
        this.prod_name = prod_name;
        this.prod_qty = prod_qty;
        this.prod_unit = prod_unit;
        this.prod_regn_date = prod_regn_date;
        this.sale_id = sale_id;
        this.contact_id = contact_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_qty() {
        return prod_qty;
    }

    public void setProd_qty(String prod_qty) {
        this.prod_qty = prod_qty;
    }

    public String getProd_unit() {
        return prod_unit;
    }

    public void setProd_unit(String prod_unit) {
        this.prod_unit = prod_unit;
    }

    public String getProd_regn_date() {
        return prod_regn_date;
    }

    public void setProd_regn_date(String prod_regn_date) {
        this.prod_regn_date = prod_regn_date;
    }

    public String getSale_id() {
        return sale_id;
    }

    public void setSale_id(String sale_id) {
        this.sale_id = sale_id;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }
}
