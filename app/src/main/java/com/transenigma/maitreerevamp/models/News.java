package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 11/12/16.
 */

public class News {
    private String news_title;
    private String news_body;
    private String news_date;

    public News(String news_title, String news_body, String
            news_date) {
        this.news_title = news_title;
        this.news_body = news_body;
        this.news_date = news_date;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_body() {
        return news_body;
    }

    public void setNews_body(String news_body) {
        this.news_body = news_body;
    }

    public String getNews_date() {
        return news_date;
    }

    public void setNews_date(String news_date) {
        this.news_date = news_date;
    }
}
