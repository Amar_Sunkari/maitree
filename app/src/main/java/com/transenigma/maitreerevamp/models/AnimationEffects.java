package com.transenigma.maitreerevamp.models;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.transenigma.maitreerevamp.R;

/**
 * Created by Amar on 11-Mar-17.
 */


public class AnimationEffects {

    public static void slide_down(Context context, View v)
    {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_down);
        if(animation!=null)
        {
            animation.reset();
        }
        if(v!=null)
        {
            v.clearAnimation();
            v.startAnimation(animation);
        }
    }

    public static void slide_up(Context context , View v)
    {
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.slide_up);
        if(animation!=null)
        {
            animation.reset();
        }
        if(v!=null)
        {
            v.clearAnimation();
            v.startAnimation(animation);
        }
    }
}
