package com.transenigma.maitreerevamp.models;

/**
 * Created by Amar on 06-Mar-17.
 */

public class PurchaseRequest {

    private String requestor_name;
    private float purchase_amount;
    private String request_status;
    private String request_date;

    public PurchaseRequest(String requestor_name, float purchase_amount, String request_status, String request_date) {
        this.requestor_name = requestor_name;
        this.purchase_amount = purchase_amount;
        this.request_status = request_status;
        this.request_date = request_date;
    }

    public String getRequestor_name() {
        return requestor_name;
    }

    public void setRequestor_name(String requestor_name) {
        this.requestor_name = requestor_name;
    }

    public float getPurchase_amount() {
        return purchase_amount;
    }

    public void setPurchase_amount(float purchase_amount) {
        this.purchase_amount = purchase_amount;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }
}
