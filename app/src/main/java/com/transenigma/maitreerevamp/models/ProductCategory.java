package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 25/2/17.
 */

public class ProductCategory {
    private String name;
    private String id;

    public ProductCategory(String name, String id) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
