package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 25/2/17.
 */

class Product {
    private String id;
    private String name;
    private String description;
    private String price;
    private String product_image_url;

    public Product(String id, String name, String description, String price, String product_image_url) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.product_image_url = product_image_url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_image_url() {
        return product_image_url;
    }

    public void setProduct_image_url(String product_image_url) {
        this.product_image_url = product_image_url;
    }
}
