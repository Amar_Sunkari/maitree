package com.transenigma.maitreerevamp.models;

/**
 * Created by Amar on 09-Mar-17.
 */

public class CartProduct {
    private String prod_img_url, prod_id, prod_name, prod_qty, prod_pts, cart_id;

    public CartProduct(String prod_img_url, String prod_id, String prod_name, String prod_qty, String prod_pts, String cart_id) {
        this.prod_img_url = prod_img_url;
        this.prod_id = prod_id;
        this.prod_name = prod_name;
        this.prod_qty = prod_qty;
        this.prod_pts = prod_pts;
        this.cart_id = cart_id;
    }

    public String getProd_img_url() {
        return prod_img_url;
    }

    public void setProd_img_url(String prod_img_url) {
        this.prod_img_url = prod_img_url;
    }

    public String getProd_id() {
        return prod_id;
    }

    public void setProd_id(String prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getProd_qty() {
        return prod_qty;
    }

    public void setProd_qty(String prod_qty) {
        this.prod_qty = prod_qty;
    }

    public String getProd_pts() {
        return prod_pts;
    }

    public void setProd_pts(String prod_pts) {
        this.prod_pts = prod_pts;
    }

    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }
}
