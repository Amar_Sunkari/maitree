package com.transenigma.maitreerevamp.models;

/**
 * Created by Amar on 10-Mar-17.
 */

public class DealerDetails {

    private String name, img_url;
    private String amount;
    private String rank;

    public DealerDetails(String name, String img_url, String amount, String rank) {
        this.name = name;
        this.img_url = img_url;
        this.amount = amount;
        this.rank = rank;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }
}
