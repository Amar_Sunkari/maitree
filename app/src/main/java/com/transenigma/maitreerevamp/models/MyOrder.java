package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 12/11/16.
 */

public class MyOrder {

    private String product_image, product_name, product_code, product_item_id, price_point, status, order_id,
            order_date,
            awb_no,
            courier_name, can_cancel;

    public MyOrder(String product_image, String product_name, String product_code, String product_item_id, String
            price_point, String status,
                   String order_id, String order_date, String awb_no, String courier_name, String can_cancel) {
        this.product_image = product_image;
        this.product_name = product_name;
        this.product_code = product_code;
        this.product_item_id = product_item_id;
        this.price_point = price_point;
        this.status = status;
        this.order_id = order_id;
        this.order_date = order_date;
        this.awb_no = awb_no;
        this.courier_name = courier_name;
        this.can_cancel = can_cancel;
    }

    public String getProduct_image() {
        return product_image;
    }

    public void setProduct_image(String product_image) {
        this.product_image = product_image;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_item_id() {
        return product_item_id;
    }

    public void setProduct_item_id(String product_item_id) {
        this.product_item_id = product_item_id;
    }

    public String getPrice_point() {
        return price_point;
    }

    public void setPrice_point(String price_point) {
        this.price_point = price_point;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getAwb_no() {
        return awb_no;
    }

    public void setAwb_no(String awb_no) {
        this.awb_no = awb_no;
    }

    public String getCourier_name() {
        return courier_name;
    }

    public void setCourier_name(String courier_name) {
        this.courier_name = courier_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getCan_cancel() {
        return can_cancel;
    }

    public void setCan_cancel(String can_cancel) {
        this.can_cancel = can_cancel;
    }
}
