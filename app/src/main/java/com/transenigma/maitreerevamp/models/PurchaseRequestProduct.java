package com.transenigma.maitreerevamp.models;

/**
 * Created by Amar on 07-Mar-17.
 */

public class PurchaseRequestProduct {

    private String product_name;
    private int quantity;
    private float sub_total;

    public PurchaseRequestProduct(String product_name, int quantity, float sub_total) {
        this.product_name = product_name;
        this.quantity = quantity;
        this.sub_total = sub_total;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getSub_total() {
        return sub_total;
    }

    public void setSub_total(float sub_total) {
        this.sub_total = sub_total;
    }
}
