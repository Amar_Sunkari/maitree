package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 2/1/17.
 */
public class CheckoutProduct {

    private String checkout_product_img;
    private String checkout_product_title;
    private String checkout_product_qty;
    private String checkout_product_pts;

    public CheckoutProduct(String checkout_product_img, String checkout_product_title, String
            checkout_product_qty, String
                                   checkout_product_pts) {
        this.checkout_product_img = checkout_product_img;
        this.checkout_product_title = checkout_product_title;
        this.checkout_product_qty = checkout_product_qty;
        this.checkout_product_pts = checkout_product_pts;
    }

    public String getCheckout_product_img() {
        return checkout_product_img;
    }

    public void setCheckout_product_img(String checkout_product_img) {
        this.checkout_product_img = checkout_product_img;
    }

    public String getCheckout_product_title() {
        return checkout_product_title;
    }

    public void setCheckout_product_title(String checkout_product_title) {
        this.checkout_product_title = checkout_product_title;
    }

    public String getCheckout_product_qty() {
        return checkout_product_qty;
    }

    public void setCheckout_product_qty(String checkout_product_qty) {
        this.checkout_product_qty = checkout_product_qty;
    }

    public String getCheckout_product_pts() {
        return checkout_product_pts;
    }

    public void setCheckout_product_pts(String checkout_product_pts) {
        this.checkout_product_pts = checkout_product_pts;
    }
}
