package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 6/4/17.
 */

public class SubDealerPerformance {

    private String subdealer_name;
    private String subdealer_code;
    private String total_sale_no;
    private String status;
    private String child_id;
    private String child_hier_id;
    private String chils_is_verified;

    public SubDealerPerformance(String subdealer_name, String subdealer_code, String total_sale_no, String status,
                                String child_id, String child_hier_id, String child_is_verified) {
        this.subdealer_name = subdealer_name;
        this.subdealer_code = subdealer_code;
        this.total_sale_no = total_sale_no;
        this.status = status;
        this.child_id = child_id;
        this.child_hier_id = child_hier_id;
        this.chils_is_verified = child_is_verified;
    }

    public String getSubdealer_name() {
        return subdealer_name;
    }

    public void setSubdealer_name(String subdealer_name) {
        this.subdealer_name = subdealer_name;
    }

    public String getSubdealer_code() {
        return subdealer_code;
    }

    public void setSubdealer_code(String subdealer_code) {
        this.subdealer_code = subdealer_code;
    }

    public String getTotal_sale_no() {
        return total_sale_no;
    }

    public void setTotal_sale_no(String total_sale_no) {
        this.total_sale_no = total_sale_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChild_id() {
        return child_id;
    }

    public void setChild_id(String child_id) {
        this.child_id = child_id;
    }

    public String getChild_hier_id() {
        return child_hier_id;
    }

    public void setChild_hier_id(String child_hier_id) {
        this.child_hier_id = child_hier_id;
    }

    public String getChils_is_verified() {
        return chils_is_verified;
    }

    public void setChils_is_verified(String chils_is_verified) {
        this.chils_is_verified = chils_is_verified;
    }
}
