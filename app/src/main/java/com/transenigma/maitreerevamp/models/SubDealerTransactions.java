package com.transenigma.maitreerevamp.models;

/**
 * Created by subhadip on 7/4/17.
 */

public class SubDealerTransactions {

    private String transc_id;
    private String product_name;
    private String product_sold;
    private String sale_date;
    private String purchased_from;
    private String dealer_code;
    private String registered_on;
    private String status;

    public SubDealerTransactions(String transc_id, String product_name, String product_sold, String sale_date, String purchased_from, String dealer_code, String registered_on, String status) {
        this.transc_id = transc_id;
        this.product_name = product_name;
        this.product_sold = product_sold;
        this.sale_date = sale_date;
        this.purchased_from = purchased_from;
        this.dealer_code = dealer_code;
        this.registered_on = registered_on;
        this.status = status;
    }

    public String getTransc_id() {
        return transc_id;
    }

    public void setTransc_id(String transc_id) {
        this.transc_id = transc_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_sold() {
        return product_sold;
    }

    public void setProduct_sold(String product_sold) {
        this.product_sold = product_sold;
    }

    public String getSale_date() {
        return sale_date;
    }

    public void setSale_date(String sale_date) {
        this.sale_date = sale_date;
    }

    public String getPurchased_from() {
        return purchased_from;
    }

    public void setPurchased_from(String purchased_from) {
        this.purchased_from = purchased_from;
    }

    public String getDealer_code() {
        return dealer_code;
    }

    public void setDealer_code(String dealer_code) {
        this.dealer_code = dealer_code;
    }

    public String getRegistered_on() {
        return registered_on;
    }

    public void setRegistered_on(String registered_on) {
        this.registered_on = registered_on;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
