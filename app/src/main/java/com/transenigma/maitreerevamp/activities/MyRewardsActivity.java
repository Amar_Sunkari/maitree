package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.MyRewardAdapter;
import com.transenigma.maitreerevamp.models.MyReward;
import com.transenigma.maitreerevamp.models.ProdCategory;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class MyRewardsActivity extends AppCompatActivity {

    private static final String TAG = MyRewardsActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    Toolbar myRewardsBottomBar;
    @BindView(R.id.bottombar_title)
    TextView myRewardsBottomBarTitle;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;
    @BindView(R.id.loadingPanelMin)
    RelativeLayout loadingMin;

    private AppCompatButton applyBtn;
    private AppCompatSeekBar ptsSeekBar;
    private Spinner prodCatSpinner;
    private TextView maxPtsLabel;
    private TextView minPtsLabel;

    private int pastVisibleItems, visibleItemCount, totalItemCount, pageNumber;
    private boolean loadMore = true;
    private boolean notProcessing = true;
    private SharedPreferences pref;
    private MyRewardAdapter myRewardAdapter;
    private ArrayAdapter<ProdCategory> categoryArrayAdapter;

    @BindView(R.id.rewards_recylerview)
    RecyclerView rewards_recyclerView;
    private LinearLayoutManager rewards_layoutManager;

    private final ArrayList<ProdCategory> categories_list = new ArrayList<>();
    private final ArrayList<MyReward> rewardsProducts_list = new ArrayList<>();

    private final String catalog_url = "http://uat-mandres.straightline.in/maitree_req/catalog";
    private String filterCatId, filterMaxPts;
    private JSONObject minMaxObj = null;
    private int maxPts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rewards);
        ButterKnife.bind(this);

        setSupportActionBar(myRewardsBottomBar);
        myRewardsBottomBarTitle.setText("My Rewards");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode


        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle("Network Error");
            networkAlert.setMessage("Please check your internet connection and try again...");
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {

            rewards_layoutManager = new LinearLayoutManager(this);
            rewards_recyclerView.setLayoutManager(rewards_layoutManager);

            getCategoryProducts("", "1", "", "", "0");
        }
    }

    private void getCategoryProducts(final String categoryId, final String pageNum, final
    String min, final String max, final String filter) {

        StringRequest catalogRequest = new StringRequest(Request.Method.POST, catalog_url, new
                Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i(TAG, "onResponse: " + response);

                        convertCategoryData(response, categories_list, rewardsProducts_list);

                        //Reward adapter
                        myRewardAdapter = new MyRewardAdapter(rewardsProducts_list,
                                MyRewardsActivity.this);

                        rewards_recyclerView.setAdapter(myRewardAdapter);
                        myRewardAdapter.notifyDataSetChanged();

                        //category adapter
                        categoryArrayAdapter = new ArrayAdapter<>(
                                getBaseContext(), R.layout.spinner_layout, categories_list
                        );
                        categoryArrayAdapter.setDropDownViewResource(android.R.layout
                                .simple_spinner_dropdown_item);

                        //scroll to latest products
                        rewards_recyclerView.scrollToPosition(rewards_layoutManager.getItemCount
                                () - 8);

                        //Detect scrolling and add more products
                        rewards_recyclerView.addOnScrollListener(new RecyclerView
                                .OnScrollListener() {

                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                if (dy > 0) {
                                    visibleItemCount = rewards_layoutManager.getChildCount();
                                    totalItemCount = rewards_layoutManager.getItemCount();
                                    pastVisibleItems = rewards_layoutManager
                                            .findFirstVisibleItemPosition();

                                    if (loadMore && notProcessing) {
                                        if ((visibleItemCount + pastVisibleItems) >=
                                                totalItemCount) {
                                            pageNumber++;

                                            if (loadMore) {
                                                getCategoryProducts("", String.valueOf
                                                                (pageNumber), "",
                                                        "", "1");
                                            }
                                            notProcessing = false;
                                            loadingMin.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            }
                        });
                        loadingMin.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("orderType", "mobileapp");
                params.put("programId", pref.getString("programId", ""));
                params.put("categoryId", categoryId); // "" for no filter, cat id for filter
                params.put("pageNumber", pageNum); // 1 for default ++ afterwards
                params.put("min", min); // 0
                params.put("max", max); // desired value
                params.put("filter", filter); //0 for no filter and 1 for filter
                params.put("returnType", returnType); //json

                return params;
            }
        };

        catalogRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(catalogRequest);
    }

    private void convertCategoryData(final String response, final ArrayList<ProdCategory>
            categories_list, final ArrayList<MyReward> rewardsProducts_list) {
        this.runOnUiThread(new Runnable() {
                               @Override
                               public void run() {
                                   JSONObject catalogObj = null;
                                   try {
                                       catalogObj = new JSONObject(response);
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }

                                   String status = null;
                                   String msg = null;

                                   try {
                                       msg = catalogObj.isNull("message") ? null : catalogObj
                                               .getString("message");
                                       status = catalogObj.isNull("status") ? null : catalogObj
                                               .getString("status");
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }

                                   if ((status != null) && status.equals("failed")) {
                                       loadMore = false;
                                       if (msg.equals("Invalid token")) {
                                           loadingMin.setVisibility(View.GONE);

                                           AlertDialog.Builder checkoutAlert = new android
                                                   .support.v7.app.AlertDialog.Builder
                                                   (MyRewardsActivity.this);
                                           checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                           checkoutAlert.setTitle("Your session has expired");
                                           checkoutAlert.setMessage("Please log in again.");
                                           checkoutAlert.setCancelable(false);
                                           checkoutAlert.setNeutralButton("Close", new
                                                   DialogInterface.OnClickListener() {
                                                       @Override
                                                       public void onClick(DialogInterface
                                                                                   dialogInterface, int i) {
                                                           final SharedPreferences.Editor editor
                                                                   = pref
                                                                   .edit();
                                                           //reset shared pref excluding fcm token
                                                           editor.putBoolean("isLoggedIn", false);
                                                           editor.putString("token", "");
                                                           editor.putString("programId", "");
                                                           editor.putString("cartCount", "");
                                                           editor.putString("profile_pic_url", "");
                                                           editor.putString("first_name", "");
                                                           editor.putString("last_name", "");
                                                           editor.putString("cartCount", "0");

                                                           editor.apply();
                                                           startActivity(new Intent
                                                                   (MyRewardsActivity
                                                                           .this, LoginActivity
                                                                           .class));
                                                           finish();
                                                       }
                                                   });
                                           AlertDialog alert = checkoutAlert.create();
                                           alert.show();

                                       } else {
                                           loadingMin.setVisibility(View.GONE);
                                           Toast.makeText(MyRewardsActivity.this, "No more " +
                                                   "product found!", Toast.LENGTH_SHORT).show();
                                       }
                                       return;
                                   }

                                   JSONArray productsArr;
                                   JSONArray prodCategoryArr;
                                   try {
                                       assert catalogObj != null;
                                       productsArr = catalogObj.getJSONArray("products");
                                       prodCategoryArr = catalogObj.getJSONArray("categories");

                                       minMaxObj = catalogObj.getJSONObject("minMax");
                                       maxPts = minMaxObj.getInt("max");

                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                       loadMore = false;
                                       return;
                                   }

                                   int productsLength = productsArr.length();
                                   int categoryLength = prodCategoryArr.length();

                                   for (int i = 0; i < productsLength; i++) {
                                       String prod_img_url;
                                       try {
                                           prod_img_url = productsArr.getJSONObject(i).getString
                                                   ("BaseUrl") + productsArr.getJSONObject(i)
                                                   .getString("ProductImage");

                                           rewardsProducts_list.add(new MyReward(prod_img_url,
                                                   productsArr.getJSONObject(i).getString
                                                           ("productId"), productsArr
                                                   .getJSONObject(i).getString("productName"),
                                                   productsArr.getJSONObject(i).getString
                                                           ("pricePoints"), productsArr
                                                   .getJSONObject(i).getJSONObject
                                                           ("ProductParams").toString(),
                                                   productsArr.getJSONObject(i).getString
                                                           ("ProductDesc")));

                                       } catch (JSONException e) {
                                           e.printStackTrace();
                                       }
                                   }

                                   if (categories_list.isEmpty()) {
                                       for (int i = 0; i < categoryLength; i++) {
                                           try {
                                               categories_list.add(new ProdCategory
                                                       (prodCategoryArr.getJSONObject(i)
                                                               .getString("categoryName"),
                                                               prodCategoryArr.getJSONObject(i)
                                                                       .getString("categoryId")));
                                           } catch (JSONException e) {
                                               e.printStackTrace();
                                           }
                                       }
                                   }
                               }
                           }
        );

        notProcessing = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater filtermenu = getMenuInflater();
        filtermenu.inflate(R.menu.filter_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.filter:
                loadingMin.setVisibility(View.VISIBLE);

                filterDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void filterDialog() {

        if (categoryArrayAdapter != null) {

            LayoutInflater inflater = getLayoutInflater();
            View filterView = inflater.inflate(R.layout.alert_dialog_editor, null);
            final AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(this);
            builder1.setView(filterView);
            builder1.setTitle("Filter Rewards");
            final AlertDialog dialog = builder1.create();
            dialog.show();

            minPtsLabel = (TextView) filterView.findViewById(R.id.minPtslabel);
            maxPtsLabel = (TextView) filterView.findViewById(R.id.maxPtsLabel);

            prodCatSpinner = (Spinner) filterView.findViewById(R.id.filter_category_spinner);
            ptsSeekBar = (AppCompatSeekBar) filterView.findViewById(R.id.pts_seekbar);
            applyBtn = (AppCompatButton) filterView.findViewById(R.id.apply_btn);

            loadingMin.setVisibility(View.GONE);
            prodCatSpinner.setAdapter(categoryArrayAdapter);
            ptsSeekBar.setMax(maxPts);

            int max = 0;
            try {
                max = Integer.parseInt(minMaxObj.getString("max"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            maxPtsLabel.setText("" + max);
            ptsSeekBar.setMax(max);

            prodCatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                    final ProdCategory prodCat = (ProdCategory) parent.getSelectedItem();
                    filterCatId = prodCat.getCategory_id();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            filterMaxPts = "";
            ptsSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    maxPtsLabel.setText("" + i);
                    filterMaxPts = "" + i;
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            applyBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    rewardsProducts_list.clear();
                    getCategoryProducts(filterCatId, "1", "0", filterMaxPts, "1");
                    dialog.dismiss();
                }
            });
        }
    }
}