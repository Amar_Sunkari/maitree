package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.ProductDetailsPagerAdapter;
import com.transenigma.maitreerevamp.fragments.ProdDetailsFragment;
import com.transenigma.maitreerevamp.fragments.ProdImagesFragment;
import com.transenigma.maitreerevamp.fragments.ProdSpecFragment;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.DashActivity.cart_count_tv;
import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;

public class ProductDetailsActivity extends AppCompatActivity {
    private static final String TAG = ProductDetailsActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    Toolbar productDetailsBottomBar;
    @BindView(R.id.bottombar_title)
    TextView productDetailsBottomBarTitle;

    @BindView(R.id.add_item)
    AppCompatImageButton addQtyBtn;
    @BindView(R.id.reduce_item)
    AppCompatImageButton removeQtyBtn;

    @BindView(R.id.product_details_intro_img)
    ImageView rewardProdDetailImage;
    @BindView(R.id.product_details_intro_title)
    TextView prodIntroName;
    @BindView(R.id.product_details_intro_pts)
    TextView prodIntroPts;

    @BindView(R.id.product_details_viewpager)
    ViewPager ProductDetailsViewpager;

    @BindView(R.id.product_details_tabs)
    TabLayout tabLayout;
    private final String add_to_cart_url = "http://uat-mandres.straightline.in/maitree_req/addcart";
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @BindView(R.id.add_to_cart_btn)
    AppCompatButton addToCartBtn;

    @BindView(R.id.cv_item_qty)
    TextView product_qty;

    private int product_qty_final;
    private int prod_qty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details_new);

        ButterKnife.bind(this);

        setSupportActionBar(productDetailsBottomBar);
        productDetailsBottomBarTitle.setText("Reward Details");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        prod_qty = Integer.parseInt(product_qty.getText().toString());

        if (prod_qty >= 1) {
            removeQtyBtn.setVisibility(View.INVISIBLE);
        } else {
            removeQtyBtn.setVisibility(View.VISIBLE);
        }

        prodIntroName.setText(getIntent().getStringExtra("prod_name"));
        prodIntroPts.setText(getIntent().getStringExtra("prod_points") + " PTS");

        addQtyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prod_qty = Integer.parseInt(product_qty.getText().toString());

                product_qty.setText("" + (prod_qty + 1));

                prod_qty = Integer.parseInt(product_qty.getText().toString());

                if (prod_qty >= 1) {
                    removeQtyBtn.setVisibility(View.VISIBLE);
                }
            }
        });

        removeQtyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prod_qty = Integer.parseInt(product_qty.getText().toString());

                if (prod_qty >= 2) {
                    product_qty.setText("" + (prod_qty - 1));
                }

                prod_qty = Integer.parseInt(product_qty.getText().toString());
                if (prod_qty < 2) {
                    removeQtyBtn.setVisibility(View.INVISIBLE);
                }
            }
        });

        tabLayout.setupWithViewPager(ProductDetailsViewpager);

        //get user pref from shared pref
        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode
        editor = pref.edit();

        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product_qty_final = Integer.parseInt(product_qty.getText().toString());
                product_qty_final = (product_qty_final <= 1) ? 1 : product_qty_final;

                final StringRequest addToCartRequest = new StringRequest(Request.Method.POST,
                        add_to_cart_url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("TAG", "onResponse: " + response);
                        JSONObject addToCartResponseObj = null;
                        int bstatus = 0;
                        try {
                            addToCartResponseObj = new JSONObject(response);
                            bstatus = addToCartResponseObj.getInt("bstatus");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if (bstatus == 1) {
                            String msg = null;
                            String total_item_count = null;
                            try {
                                msg = addToCartResponseObj.getString("message");
                                total_item_count = addToCartResponseObj.getJSONObject
                                        ("control").getString("total_count");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

//                            int cart_count = Integer.parseInt(pref.getString("cartCount", null));
//                            cart_count = cart_count + 1;
                            editor.putString("cartCount", total_item_count);
                            editor.apply();

                            supportInvalidateOptionsMenu();
                            Toast.makeText(ProductDetailsActivity.this, msg, Toast
                                    .LENGTH_SHORT).show();
                        } else {
                            String msg = null;
                            try {
                                msg = addToCartResponseObj.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (msg.equals("Invalid token")) {
                                AlertDialog.Builder checkoutAlert = new android.support
                                        .v7.app.AlertDialog.Builder(ProductDetailsActivity
                                        .this);
                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                checkoutAlert.setTitle("Your session has expired");
                                checkoutAlert.setMessage("Please log in again.");
                                checkoutAlert.setCancelable(false);
                                checkoutAlert.setNeutralButton("Close", new DialogInterface
                                        .OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int
                                            i) {
                                        final SharedPreferences.Editor editor = pref.edit();
                                        //reset shared pref excluding fcm token
                                        editor.putBoolean("isLoggedIn", false);
                                        editor.putString("token", "");
                                        editor.putString("programId", "");
                                        editor.putString("cartCount", "");
                                        editor.putString("profile_pic_url", "");
                                        editor.putString("first_name", "");
                                        editor.putString("last_name", "");


                                        editor.apply();
                                        startActivity(new Intent(ProductDetailsActivity
                                                .this, LoginActivity.class));
                                        finish();
                                    }
                                });
                                AlertDialog alert = checkoutAlert.create();
                                alert.show();

                            } else {
                                Toast.makeText(ProductDetailsActivity.this, "Something " +
                                        "went wrong", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("token", pref.getString("token", ""));
                        params.put("APIkey", APIkey);
                        params.put("prod_id", getIntent().getStringExtra("prod_id"));
                        params.put("price", "");
                        params.put("prod_name", getIntent().getStringExtra("prod_name"));
                        params.put("price_in_points", getIntent().getStringExtra
                                ("prod_points"));
                        params.put("quantity", "" + product_qty_final);
                        params.put("programId", "4");
                        return params;
                    }
                };

                addToCartRequest.setRetryPolicy(new DefaultRetryPolicy(
                        5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                        (addToCartRequest);
            }
        });


        String img_url = getIntent().getStringExtra("prod_img");
        String prod_details = getIntent().getStringExtra("prod_details");
        String prod_params = getIntent()
                .getStringExtra("prod_params");
        Log.i(TAG, "onCreate: " + img_url + " " + prod_details + " " + prod_params);

        if (img_url != null) {
            Picasso.with(ProductDetailsActivity.this).load(img_url).into(rewardProdDetailImage);
        }

        ProductDetailsPagerAdapter productDetailsPagerAdapter = new ProductDetailsPagerAdapter
                (getSupportFragmentManager());

        ProdDetailsFragment prodDetailsFrag = new ProdDetailsFragment().newInstance(prod_details);
        ProdImagesFragment prodScreenShotFrag = new ProdImagesFragment().newInstance(img_url);
        ProdSpecFragment prodSpecFrag = new ProdSpecFragment().newInstance(prod_params);

        productDetailsPagerAdapter.addFragments(prodScreenShotFrag);
        productDetailsPagerAdapter.addFragments(prodDetailsFrag);
        productDetailsPagerAdapter.addFragments(prodSpecFrag);

        ProductDetailsViewpager.setAdapter(productDetailsPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart_popup_menu:
                startActivity(new Intent(getBaseContext(), CartActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem actionViewItem = menu.findItem(R.id.cart_popup_menu);
        View v = MenuItemCompat.getActionView(actionViewItem);

        ImageView cart_iv = (ImageView) v.findViewById(R.id.cart_img);
        cart_count_tv = (TextView) v.findViewById(R.id.cartCountTextView);

        int cart_count = Integer.parseInt(pref.getString("cartCount", "0"));
        if (cart_count > 0) {
            cart_count_tv.setText("" + cart_count);
        } else {
            cart_count_tv.setText("0");
        }

        cart_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ProductDetailsActivity.this, CartActivity.class));
            }
        });

        return super.onPrepareOptionsMenu(menu);
    }
}