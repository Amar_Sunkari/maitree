package com.transenigma.maitreerevamp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyOrderDetailsActivity extends AppCompatActivity {

    @BindView(R.id.bottombar)
    Toolbar orderDetailsBottomBar;
    @BindView(R.id.bottombar_title)
    TextView orderDetailsBottomBarTitle;

    private AppCompatImageView orderImage;
    private TextView order_id;
    private TextView order_date;
    private TextView prod_name;
    private TextView prod_code;
    private TextView price_point;
    private TextView prod_overlay_status;
    private TextView awb_no;
    private TextView courier_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        ButterKnife.bind(this);

        setSupportActionBar(orderDetailsBottomBar);
        orderDetailsBottomBarTitle.setText("Order Details");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        orderImage = (AppCompatImageView) findViewById(R.id.order_details_img);

        prod_overlay_status = (TextView) findViewById(R.id.overlay_status);

        order_id = (TextView) findViewById(R.id.tv_order_id);
        order_date = (TextView) findViewById(R.id.tv_order_details_order_date);
        prod_name = (TextView) findViewById(R.id.tv_order_details_product_name);
        prod_code = (TextView) findViewById(R.id.tv_order_details_product_code);
        price_point = (TextView) findViewById(R.id.tv_order_details_price_points);
        awb_no = (TextView) findViewById(R.id.tv_order_details_awb_no);
        courier_name = (TextView) findViewById(R.id.tv_order_details_courier_name);

        if (!getIntent().getStringExtra("order_image").equals("")) {
            Picasso.with(getBaseContext()).load(getIntent().getStringExtra("order_image")).into(orderImage);
        }

        order_id.setText(getIntent().getStringExtra("order_id"));
        order_date.setText(getIntent().getStringExtra("order_date"));
        prod_name.setText(getIntent().getStringExtra("prod_name"));
        prod_code.setText(getIntent().getStringExtra("prod_code"));
        price_point.setText(getIntent().getStringExtra("price_point"));
        prod_overlay_status.setText(getIntent().getStringExtra("prod_status"));
        awb_no.setText(getIntent().getStringExtra("awb_no").equals("null") ? "Not Available" : getIntent().getStringExtra("awb_no"));
        courier_name.setText(getIntent().getStringExtra("courier_name"));
    }
}