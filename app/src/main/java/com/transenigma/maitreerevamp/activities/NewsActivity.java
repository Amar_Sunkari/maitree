package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.NewsAdapter;
import com.transenigma.maitreerevamp.models.News;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class NewsActivity extends AppCompatActivity {

    private final String TAG = NewsActivity.class.getSimpleName();

    private SharedPreferences pref;
    private final ArrayList<News> news_list = new ArrayList<>();
    private final String news_url = "http://uat-mandres.straightline.in/maitree_req/content";
    private RelativeLayout loading;
    private Toolbar newsBottomBar;
    private TextView newsBottomBarTitle;

    private RecyclerView news_recyclerView;
    private RecyclerView.LayoutManager newsLayoutManager;
    private NewsAdapter newsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        newsBottomBar = (Toolbar) findViewById(R.id.bottombar);
        newsBottomBarTitle = (TextView) findViewById(R.id.bottombar_title);

        setSupportActionBar(newsBottomBar);
        newsBottomBarTitle.setText(R.string.ACT_NEWS_UPDATES_TITLE);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_mat);

        loading = (RelativeLayout) findViewById(R.id.loadingPanel);
        loading.setVisibility(View.VISIBLE);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        //Populate notification recyclerView
        news_recyclerView = (RecyclerView) findViewById(R.id
                .notifications_recyclerView);
        newsLayoutManager = new LinearLayoutManager(this);
        news_recyclerView.setLayoutManager(newsLayoutManager);

        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle(R.string.NETWORK_ERROR_TITLE);
            networkAlert.setMessage(R.string.NETWORK_ERROR_MSG);
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {

            StringRequest notificationRequest = new StringRequest(Request.Method.POST,
                    news_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i(TAG, "onResponse: " + response);
                    JSONArray notificationArr;
                    JSONObject notificationObjAll;
                    try {
                        notificationObjAll = new JSONObject(response);

                        int bstatus = notificationObjAll.getInt("bstatus");

                        if (bstatus == 1) {
                            notificationArr = notificationObjAll.getJSONArray("contents");
                            int notificationArrLength = notificationArr.length();
                            String title;
                            String content;
                            String contentDate;

                            for (int i = 0; i < notificationArrLength; i++) {
                                JSONObject notificationObj;
                                notificationObj = notificationArr.getJSONObject(i);
                                title = notificationObj.getString("title");
                                content = notificationObj.getString("details");
                                contentDate = notificationObj.getString("contentDate");
                                News news = new News(title, content,
                                        contentDate);

                                news_list.add(news);
                            }
                            newsAdapter = new NewsAdapter(news_list,
                                    getBaseContext());

                            news_recyclerView.setAdapter(newsAdapter);
                            newsAdapter.notifyDataSetChanged();

                            Log.i(TAG, "onResponse: " + news_list);

                            loading.setVisibility(View.GONE);
                        } else {
                            JSONObject notificationMsgObj;
                            String msg = null;
                            try {
                                notificationMsgObj = new JSONObject(response);
                                msg = notificationMsgObj.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            loading.setVisibility(View.GONE);

                            if (msg.equals("Invalid token")) {
                                AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                        .AlertDialog.Builder(NewsActivity.this);
                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                checkoutAlert.setTitle("Your session has expired");
                                checkoutAlert.setMessage("Please log in again.");
                                checkoutAlert.setCancelable(false);
                                checkoutAlert.setNeutralButton("Close", new DialogInterface
                                        .OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final SharedPreferences.Editor editor = pref.edit();
                                        //reset shared pref excluding fcm token
                                        editor.putBoolean("isLoggedIn", false);
                                        editor.putString("token", "");
                                        editor.putString("programId", "");
                                        editor.putString("cartCount", "");
                                        editor.putString("profile_pic_url", "");
                                        editor.putString("first_name", "");
                                        editor.putString("last_name", "");
                                        editor.putString("cartCount", "0");

                                        editor.apply();
                                        startActivity(new Intent(NewsActivity.this, LoginActivity
                                                .class));
                                        finish();
                                    }
                                });
                                AlertDialog alert = checkoutAlert.create();
                                alert.show();
                            }
                        }
                    } catch (JSONException error) {
                        error.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("token", pref.getString("token", ""));
                    params.put("APIkey", APIkey);
                    params.put("programId", pref.getString("programId", ""));
                    params.put("contentCode", "heroesDisclaimer");
                    params.put("returnType", returnType);
                    return params;
                }
            };

            notificationRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                    (notificationRequest);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!pref.getBoolean("isLoggedIn", false)) {
            finish();
        }
    }
}