package com.transenigma.maitreerevamp.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;

public class AboutProgramActivity extends AppCompatActivity {

    private Toolbar faqToolbar;
    private TextView faqToolbarTitle;

    private WebView faqsWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_program);

        faqToolbar = (Toolbar) findViewById(R.id.bottombar);
        faqToolbarTitle = (TextView) findViewById(R.id.bottombar_title);

        setSupportActionBar(faqToolbar);

        faqToolbarTitle.setText("About Program");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        faqsWebView = (WebView) findViewById(R.id.about_program_webview);

        faqsWebView.loadDataWithBaseURL(null, getResources().getString(R.string.faqs_new),
                "text/html; charset=utf-8", "UTF-8", null);
//        faqsWebView.setBackgroundColor(getResources().getColor(R.color.colorTransparent));

    }
}
