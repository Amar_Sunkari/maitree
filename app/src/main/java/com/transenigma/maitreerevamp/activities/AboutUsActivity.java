package com.transenigma.maitreerevamp.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AboutUsActivity extends AppCompatActivity {

    @BindView(R.id.bottombar)
    Toolbar aboutUsBottomBar;
    @BindView(R.id.bottombar_title)
    TextView aboutUsBottomBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        ButterKnife.bind(this);

        setSupportActionBar(aboutUsBottomBar);
        aboutUsBottomBarTitle.setText("About Us");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void dial_contact(View view) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse(getString(R.string.CONTACT_TEL_LINK)));
        startActivity(intent);
    }

    public void mail_contact(View view) {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(getString(R.string.CONTACT_EMAIL_LINK)));
        startActivity(intent);
    }
}