package com.transenigma.maitreerevamp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.orgId;

public class UploadImgActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String image_upload_url = "http://uat-mandres.straightline" +
            ".in/maitree_req/change_profile_image";
    Toolbar uploadImgToolBar;
    TextView uploadImgToolBarTitle;
    private SharedPreferences pref;
    private Button buttonChoose;
    private Button buttonUpload;
    private ImageView imageView;
    private Bitmap bitmap;
    private final int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_img);

//        uploadImgToolBar = (Toolbar) findViewById(R.id.bottombar_layout);
//        uploadImgToolBarTitle = (TextView) findViewById(R.id.bottombar_title);

//        setSupportActionBar(uploadImgToolBar);
//        uploadImgToolBarTitle.setText("Upload Image");
//
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        buttonChoose = (Button) findViewById(R.id.buttonChoose);
        buttonUpload = (Button) findViewById(R.id.buttonUpload);
        imageView = (ImageView) findViewById(R.id.imageView);

        buttonChoose.setOnClickListener(this);
        buttonUpload.setOnClickListener(this);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data
                .getData() != null) {
            Uri filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == buttonChoose) {
            showFileChooser();
        }

        if (view == buttonUpload) {
            if (bitmap == null) {
                Toast imgToast = Toast.makeText(UploadImgActivity.this, "Please select an image " +
                        "first!", Toast.LENGTH_LONG);
                imgToast.setGravity(Gravity.CENTER, 0, 0);
                imgToast.show();
            } else {
                uploadImage();
            }
        }
    }

    private String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    private void uploadImage() {
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please " +
                "wait...", false, false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, image_upload_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        loading.dismiss();
                        Log.i("RES", "onResponse: " + response);

                        JSONObject uploadObj = null;
                        try {
                            uploadObj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        int bstatus = 0;
                        try {
                            bstatus = uploadObj.getInt("bstatus");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String msg = null;
                        if (bstatus == 1) {
                            try {
                                msg = uploadObj.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (msg.equals("Image Updated Successfully")) {
                                Toast imgMsgToast = Toast.makeText(UploadImgActivity.this, msg,
                                        Toast.LENGTH_LONG);
                                imgMsgToast.setGravity(Gravity.CENTER, 0, 0);
                                imgMsgToast.show();
                                finish();
                                startActivity(new Intent(UploadImgActivity.this,
                                        MyProfileActivity.class));
                            } else {
                                Toast.makeText(UploadImgActivity.this, "Something Went Wrong! " +
                                        "Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        } else {

                            if (msg.equals("Invalid token")) {
                                AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                        .AlertDialog.Builder(UploadImgActivity.this);
                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                checkoutAlert.setTitle("Your session has expired");
                                checkoutAlert.setMessage("Please log in again.");
                                checkoutAlert.setCancelable(false);
                                checkoutAlert.setNeutralButton("Close", new DialogInterface
                                        .OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final SharedPreferences.Editor editor = pref.edit();
//reset shared pref excluding fcm token
                                        editor.putBoolean("isLoggedIn", false);
                                        editor.putString("token", "");
                                        editor.putString("programId", "");
                                        editor.putString("cartCount", "");
                                        editor.putString("profile_pic_url", "");
                                        editor.putString("first_name", "");
                                        editor.putString("last_name", "");
                                        editor.putString("cartCount", "0");

                                        editor.apply();
                                        Intent intent = new Intent(UploadImgActivity.this,
                                                LoginActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                                .FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                });
                                AlertDialog alert = checkoutAlert.create();
                                alert.show();
                            } else {
                                Toast.makeText(UploadImgActivity.this, msg, Toast.LENGTH_SHORT)
                                        .show();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Log.i("ERR", "onErrorResponse: " + volleyError.toString());
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);
                //Creating parameters
                Map<String, String> params = new Hashtable<>();
                //Adding parameters
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("programId", pref.getString("programId", ""));
                params.put("orgId", orgId);
                params.put("c_file", image);

                //returning parameters
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
    }
}