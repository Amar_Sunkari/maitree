package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class MyProfileActivity extends AppCompatActivity {

    private static final String TAG = MyProfileActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    public Toolbar myProfileBottomBar;
    @BindView(R.id.bottombar_title)
    TextView myProfileBottomBarTitle;
    @BindView(R.id.change_password_button)
    public Button change_password;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;

    @BindView(R.id.points_history_btn)
    AppCompatButton pointsHistoryBtn;

    private AppCompatImageButton uploadImgBtn;

    private TextView name, userName, tier,
            addr_line_1, addr_line_2, user_city, user_state,
            user_pincode;
    private ImageView profile_pic;
    private final String user_profile_url = "http://uat-mandres.straightline.in/maitree_req/profile";

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        setSupportActionBar(myProfileBottomBar);

        myProfileBottomBarTitle.setText("My Profile");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyProfileActivity.this, ChangePasswordActivity.class));
            }
        });

        uploadImgBtn = (AppCompatImageButton) findViewById(R.id.upload_img_act_btn);
        profile_pic = (ImageView) findViewById(R.id.myProfile_pic);
        name = (TextView) findViewById(R.id.myprofile_username);
        userName = (TextView) findViewById(R.id.user_name);

        tier = (TextView) findViewById(R.id.tier);

        addr_line_1 = (TextView) findViewById(R.id.address_line_one);
        addr_line_2 = (TextView) findViewById(R.id.address_line_two);

        user_city = (TextView) findViewById(R.id.city);
        user_state = (TextView) findViewById(R.id.state);
        user_pincode = (TextView) findViewById(R.id.pincode);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0);
        // 0 - for private mode
        editor = pref.edit();

        uploadImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyProfileActivity.this, UploadImgActivity.class));
            }
        });

        pointsHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyProfileActivity.this, PointsHistoryActivity.class));
            }
        });

        loading.setVisibility(View.VISIBLE);

        StringRequest profileRequest = new StringRequest(Request.Method.POST, user_profile_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.i(TAG, "onResponse: " + response);

                        try {
                            JSONObject userProfileObj = new JSONObject(response);
                            int bstatus = userProfileObj.getInt("bstatus");

                            if (bstatus == 1) {
                                JSONObject profileObj = userProfileObj.getJSONObject("profile");
                                JSONObject pointsObj = userProfileObj.getJSONObject("points");

                                if (!profileObj.getString("profile_pic").equals("")) {
                                    String profile_pic_url = profileObj.getString("BaseUrl")
                                            + profileObj.getString("profile_pic");

                                    editor.putString("profile_pic_url", profile_pic_url);
                                    editor.apply();
                                }

                                if (!pref.getString("profile_pic_url", "").equals("")) {
                                    Picasso.with(getBaseContext()).load(pref.getString
                                            ("profile_pic_url", "")).into
                                            (profile_pic);
                                }

                                name.setText(profileObj.getString("firstName") + " " +
                                        profileObj.getString("lastName"));
                                userName.setText(profileObj.getString("ID"));
                                tier.setText(profileObj.getString("tier"));

                                addr_line_1.setText(profileObj.getString("addrLine1"));
                                addr_line_2.setText(profileObj.getString("addrLine2"));

                                user_city.setText(profileObj.getString("City"));
                                user_state.setText(profileObj.getString("State"));
                                user_pincode.setText(profileObj.getString("Pin"));

                                loading.setVisibility(View.GONE);

                            } else {

                                loading.setVisibility(View.GONE);
                                String msg = userProfileObj.getString("message");

                                if (msg.equals("Invalid token")) {
                                    AlertDialog.Builder checkoutAlert = new android.support
                                            .v7.app.AlertDialog.Builder(MyProfileActivity.this);
                                    checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                    checkoutAlert.setTitle("Your session has expired");
                                    checkoutAlert.setMessage("Please log in again.");
                                    checkoutAlert.setCancelable(false);
                                    checkoutAlert.setNeutralButton("Close", new
                                            DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface,
                                                                    int i) {
                                                    final SharedPreferences.Editor editor = pref
                                                            .edit();
                                                    //reset shared pref excluding fcm token
                                                    editor.putBoolean("isLoggedIn", false);
                                                    editor.putString("token", "");
//                                                    editor.putString("programId", "");
//                                                    editor.putString("profile_pic_url", "");
//                                                    editor.putString("first_name", "");
//                                                    editor.putString("last_name", "");
//                                                    editor.putString("cartCount", "0");

                                                    editor.apply();
                                                    startActivity(new Intent(MyProfileActivity.this,
                                                            LoginActivity.class));
                                                    finish();
                                                }
                                            });
                                    AlertDialog alert = checkoutAlert.create();
                                    alert.show();

                                } else {

                                    loading.setVisibility(View.GONE);
                                    Toast.makeText(MyProfileActivity.this, "" + msg, Toast
                                            .LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loading.setVisibility(View.GONE);

                            Toast.makeText(MyProfileActivity.this, R.string.NETWORK_ERROR_MSG, Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText(MyProfileActivity.this, R.string.NETWORK_ERROR_MSG, Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("contentCode", "lafargeMaitreeProfile");
                params.put("returnType", returnType);
                params.put("programId", pref.getString("programId", ""));

                return params;
            }
        };

        profileRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(profileRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.logout, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:

                AlertDialog.Builder checkoutAlert = new android.support.v7.app.AlertDialog.Builder
                        (MyProfileActivity.this);
                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                checkoutAlert.setTitle("Confirm Logout");
                checkoutAlert.setMessage("Are you sure you want to Logout?");
                checkoutAlert.setCancelable(false);
                checkoutAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        editor.putBoolean("isLoggedIn", false);
                        editor.putString("token", "");
                        editor.putString("programId", "");
                        editor.putString("cartCount", "0");
                        editor.putString("profile_pic_url", "");
                        editor.putString("first_name", "");
                        editor.putString("last_name", "");
                        editor.commit();

                        Intent intent = new Intent(MyProfileActivity.this, LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                                .FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });
                checkoutAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                AlertDialog alert = checkoutAlert.create();
                alert.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}