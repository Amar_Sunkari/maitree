package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.ApprovedSalesAdapter;
import com.transenigma.maitreerevamp.models.ApprovedSaleCategory;
import com.transenigma.maitreerevamp.models.ApprovedSales;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class ApproveSaleActivity extends AppCompatActivity implements View.OnLongClickListener {

    private static final String TAG = ApproveSaleActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    Toolbar approvedSaleBottomBar;

    @BindView(R.id.bottombar_title)
    TextView approvedSaleBottomBarTitle;

    private SharedPreferences pref;

    @BindView(R.id.approved_sale_recyclerView)
    RecyclerView approvedSaleRecyclerView;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;

    @BindView(R.id.loadingPanelMin)
    RelativeLayout loadingMin;

    private RecyclerView.LayoutManager layoutManager;

    @BindView(R.id.cat_search_btn)
    AppCompatButton catSearchBtn;

    private RecyclerView.Adapter adapter;

    private AppCompatButton filterApplyBtn;
    @BindView(R.id.approve_btn)
    AppCompatButton approveBtn;
    @BindView(R.id.reject_btn)
    AppCompatButton rejectBtn;

    @BindView(R.id.filter_category_spinner)
    Spinner filterSpinner;

    @BindView(R.id.approved_btn_container)
    LinearLayout approveBtnContainer;

    private Spinner bagOptionsSpinner;

    private String root_id_final;
    private String search_type = "";

    private ArrayAdapter<ApprovedSaleCategory> approvedSaleCategoryArrayAdapter;

    private final ArrayList<ApprovedSaleCategory> approvedSaleCategories = new ArrayList<>();

    private final ArrayList<ApprovedSales> approvedSales = new ArrayList<>();
    private final ArrayList<ApprovedSales> selection_list = new ArrayList<>();
    private int count = 0;

    private final String[] bag_operations = new String[]{"Equals To", "Less Than", "Greater Than"};

    private final String approved_sale_url = "http://uat-mandres.straightline.in/maitree_req/sale_confirmation";

    private final String sales_contact_url = "http://uat-mandres.straightline.in/maitree_req/getSalesContact";

    public boolean is_in_action_mode = false;

    @BindView(R.id.count_selected)
    TextView counterTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approve_sale);

        ButterKnife.bind(this);

        setSupportActionBar(approvedSaleBottomBar);

        approvedSaleBottomBarTitle.setText("Approve Sales");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        layoutManager = new LinearLayoutManager(this);
        approvedSaleRecyclerView.setLayoutManager(layoutManager);

        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle(R.string.NETWORK_ERROR_TITLE);
            networkAlert.setMessage(R.string.NETWORK_ERROR_MSG);
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {
            getApproveSaleList("", "", "", "", "", "");
            loading.setVisibility(View.GONE);
            loadingMin.setVisibility(View.GONE);
        }

        filterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final ApprovedSaleCategory approvedSaleCat = (ApprovedSaleCategory) parent.getSelectedItem();
                root_id_final = approvedSaleCat.getCat_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        catSearchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                approvedSales.clear();
                getApproveSaleList(root_id_final, "", "", "", "", "");

                getSalesContact(root_id_final);
            }
        });

        approveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selection_list.size() == 0) {
                    Toast.makeText(ApproveSaleActivity.this, "Please select a sale to Approve!", Toast.LENGTH_SHORT).show();
                } else {

                    JSONObject saleIdObj = new JSONObject();
                    JSONObject contactIdObj = new JSONObject();

                    for (int i = 0; i < selection_list.size(); i++) {
                        try {
                            saleIdObj.put("" + i, selection_list.get(i).getSale_id());
                            contactIdObj.put("" + i, selection_list.get(i).getContact_id());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    // 1 for approve
                    getApproveSaleList(root_id_final, "1", "", "", saleIdObj.toString(), contactIdObj.toString());
                    //      getApproveSaleList(root_id, res_mode, seach_type, bag_nos, saleIdObj, contactIdObj);
//                    Log.i(TAG, "onClick: saleId " + saleIdObj + " con " + contactIdObj);
                    clearActionMode();
                }
            }
        });

        rejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selection_list.size() == 0) {
                    Toast.makeText(ApproveSaleActivity.this, "Please select a sale to Reject!", Toast.LENGTH_SHORT)
                            .show();
                } else {

                    JSONObject saleIdObj = new JSONObject();
                    JSONObject contactIdObj = new JSONObject();

                    for (int i = 0; i < selection_list.size(); i++) {
                        try {
                            saleIdObj.put("" + i, selection_list.get(i).getSale_id());
                            contactIdObj.put("" + i, selection_list.get(i).getContact_id());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    // 0 for reject
                    getApproveSaleList(root_id_final, "0", "", "", saleIdObj.toString(), contactIdObj.toString());
                    //      getApproveSaleList(root_id, res_mode, seach_type, bag_nos, saleIdObj, contactIdObj);
                    clearActionMode();

                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater filterMenu = getMenuInflater();
        filterMenu.inflate(R.menu.filter_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.filter:
                filterDialog();
                return true;
            case android.R.id.home:
                if (is_in_action_mode) {
                    clearActionMode();
                    adapter.notifyDataSetChanged();
                    return true;
                } else {
                    onBackPressed();
                    return true;
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void filterDialog() {
//        if (categoryArrayAdapter != null) {
        LayoutInflater inflater = getLayoutInflater();
        View filterView = inflater.inflate(R.layout.approved_sale_filter_dialog, null);
        final AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(this);
        builder1.setView(filterView);
        builder1.setTitle("Filter Sales");
        final AlertDialog dialog = builder1.create();
        dialog.show();

        EditText noOfBags;

        noOfBags = (EditText) filterView.findViewById(R.id.no_of_bags);

        bagOptionsSpinner = (Spinner) filterView.findViewById(R.id.filter_bag_type_spinner);

        //Bags options
        ArrayAdapter<String> bagOptionsArray = new ArrayAdapter<>(ApproveSaleActivity.this, android.R.layout
                .simple_spinner_item, bag_operations);
        bagOptionsArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        bagOptionsSpinner.setAdapter(bagOptionsArray);

        bagOptionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.i(TAG, "onItemSelected: OPtion " + parent.getItemAtPosition(position));

                if (parent.getItemAtPosition(position).equals("Equals To")) {
                    search_type = "equal";
                } else if (parent.getItemAtPosition(position).equals("Greater Than")) {
                    search_type = "greater";
                } else if (parent.getItemAtPosition(position).equals("Less Than")) {
                    search_type = "lesser";
                } else {
                    search_type = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        filterApplyBtn = (AppCompatButton) filterView.findViewById(R.id.filter_apply_btn);

        final String bagNos = noOfBags.getText().toString();

        filterApplyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                approvedSales.clear();
                getApproveSaleList(root_id_final, "", search_type, bagNos, "", "");
                adapter.notifyDataSetChanged();
                loadingMin.setVisibility(View.GONE);
                dialog.dismiss();
            }
        });
    }

    private void getApproveSaleList(final String root_id, final String res_mode_final, final String search_type_final,
                                    final
                                    String bag_nos, final String saleIdObjFinal, final String contactIdObjFinal) {

        loadingMin.setVisibility(View.VISIBLE);
/*
        params.put("token", pref.getString("token", ""));
        params.put("APIkey", APIkey);
        params.put("rootId", root_id_final);      //eng or sub dealer
        params.put("row_checked", saleIdObjFinal);  //Pass sale_id indexed arr
        params.put("contact_id", contactIdObjFinal);   // pass contact_id indexed arr
        params.put("search_type", search_type_final);      // equal or greater or lesser
        params.put("bags", bag_nos);            //for filter
        params.put("created_from", "");
        params.put("created_to", "");
        params.put("contact_str", "");          //replace with contact_title = "Mr."
        params.put("res_mode", res_mode_final);   // 1 for accept 0 for reject
        params.put("returnType", returnType);
*/
        Log.i(TAG, "getApproveSaleList: Params : token " + pref.getString("token", "") + "APIkey" + APIkey + " " +
                " res_mode = " + res_mode_final + "root_id" + root_id_final + "row_checked " + saleIdObjFinal + " " +
                "contact_id " +
                contactIdObjFinal);


        StringRequest approvedSaleRequest = new StringRequest(Request.Method.POST,
                approved_sale_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);

                JSONObject approvedSalesObjAll = null;
                int bstatus = 0;
                try {
                    approvedSalesObjAll = new JSONObject(response);

                    bstatus = approvedSalesObjAll.getInt("bstatus");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                JSONObject saleDataObjAll = null;
                JSONArray saleDataArr = null;

                if (bstatus == 1) {
                    try {
                        saleDataObjAll = new JSONObject(response);
                        saleDataArr = saleDataObjAll.getJSONArray("sale_data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String msg;
                    try {
                        msg = saleDataObjAll.getString("message");
                    } catch (JSONException e) {
//                        e.printStackTrace();
                        msg = "";
                    }

                    if (!msg.equals("")) {
                        Toast.makeText(ApproveSaleActivity.this, msg, Toast.LENGTH_LONG).show();
                    }

                    populateSaleDataList(saleDataArr);

                    loadingMin.setVisibility(View.GONE);

                } else {
                    JSONObject rootCatObj = null;

                    try {
                        rootCatObj = approvedSalesObjAll.getJSONObject("root_list");
                        saleDataArr = approvedSalesObjAll.getJSONArray("sale_data");
                    } catch (JSONException e) {
//                        e.printStackTrace();
                    }

                    if (saleDataArr != null && saleDataArr.length() > 0) {
                        populateSaleDataList(saleDataArr);
                    }

                    if (approvedSaleCategories.isEmpty()) {
                        Iterator<String> iter = rootCatObj.keys();
                        while (iter.hasNext()) {
                            String key = iter.next();
                            try {
                                String value = rootCatObj.getString(key);

                                approvedSaleCategories.add(new ApprovedSaleCategory(value, key));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    approvedSaleCategoryArrayAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line,
                            approvedSaleCategories);
                    approvedSaleCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    filterSpinner.setAdapter(approvedSaleCategoryArrayAdapter);

//                    getSalesContact(root_id_final);

                    loadingMin.setVisibility(View.GONE);

                    String msg;
                    try {
                        msg = approvedSalesObjAll.getString("message");
                    } catch (JSONException e) {
//                    e.printStackTrace();
                        msg = "";   //no msg handling
                    }

                    if (msg.equals("Invalid token")) {
                        AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                .AlertDialog.Builder(ApproveSaleActivity.this);
                        checkoutAlert.setIcon(R.drawable.ic_error_outline);
                        checkoutAlert.setTitle("Your session has expired");
                        checkoutAlert.setMessage("Please log in again.");
                        checkoutAlert.setCancelable(false);
                        checkoutAlert.setNeutralButton("Close", new DialogInterface
                                .OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                final SharedPreferences.Editor editor = pref.edit();
                                //reset shared pref excluding fcm token
                                editor.putBoolean("isLoggedIn", false);
                                editor.putString("token", "");
                                editor.putString("programId", "");
                                editor.putString("cartCount", "");
                                editor.putString("profile_pic_url", "");
                                editor.putString("first_name", "");
                                editor.putString("last_name", "");
                                editor.putString("cartCount", "0");

                                editor.apply();
                                startActivity(new Intent(ApproveSaleActivity.this, LoginActivity
                                        .class));
                                finish();
                            }
                        });
                        AlertDialog alert = checkoutAlert.create();
                        alert.show();
                    }
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                loadingMin.setVisibility(View.GONE);
            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("rootId", root_id);      //eng or sub dealer
                params.put("row_checked", saleIdObjFinal);  //Pass sale_id indexed arr
                params.put("contact_id", contactIdObjFinal);   // pass contact_id indexed arr
                params.put("search_type", search_type_final);      // equal or greater or lesser
                params.put("bags", bag_nos);            //for filter
                params.put("created_from", "");
                params.put("created_to", "");
                params.put("contact_str", "");          //replace with contact_title = "Mr."
                params.put("res_mode", res_mode_final);   // 1 for accept 0 for reject
                params.put("returnType", returnType);
                return params;
            }
        };

        approvedSaleRequest.setRetryPolicy(new

                DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        VolleySingleton.getInstance(

                getApplicationContext()).

                addToRequestQueue(approvedSaleRequest);
    }

    private void getSalesContact(final String root_id) {

        StringRequest salesContactRequest = new StringRequest(Request.Method.POST,
                sales_contact_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: Sales contact list " + response);

                /*

                JSONObject approvedSalesObjAll = null;
                int bstatus = 0;
                try {
                    approvedSalesObjAll = new JSONObject(response);

                    bstatus = approvedSalesObjAll.getInt("bstatus");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (bstatus == 1) {

                    JSONObject saleDataObjAll;
                    JSONArray saleDataArr = null;
                    try {
                        saleDataObjAll = new JSONObject(response);
                        saleDataArr = saleDataObjAll.getJSONArray("sale_data");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    int saleDataLength = saleDataArr.length();

                    if (saleDataLength > 0) {

                        approveBtnContainer.setVisibility(View.VISIBLE);

                        for (int i = 0; i < saleDataLength; i++) {
                            try {
                                approvedSales.add(new ApprovedSales(saleDataArr.getJSONObject(i).getString
                                        ("Contact_Name"), saleDataArr.getJSONObject(i).getString("ID"), saleDataArr
                                        .getJSONObject(i).getString("Product"), saleDataArr.getJSONObject(i).getString
                                        ("Bags_Sold"), saleDataArr.getJSONObject(i).getString("unit_name"), saleDataArr
                                        .getJSONObject(i).getString("Sale_Registered_On"), saleDataArr.getJSONObject
                                        (i).getString("sale_id"), saleDataArr.getJSONObject(i).getString("contact_id")));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapter = new ApprovedSalesAdapter(approvedSales, ApproveSaleActivity.this);
                        approvedSaleRecyclerView.setAdapter(adapter);
                    } else {
                        approveBtnContainer.setVisibility(View.GONE);
                        Toast.makeText(ApproveSaleActivity.this, "No sales data found for the selected category!", Toast
                                .LENGTH_SHORT).show();
                    }

                } else {
                    JSONObject rootCatObj = null;

                    try {
                        rootCatObj = approvedSalesObjAll.getJSONObject("root_list");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Iterator<String> iter = rootCatObj.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {
                            String value = rootCatObj.getString(key);

                            approvedSaleCategories.add(new ApprovedSaleCategory(value, key));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    approvedSaleCategoryArrayAdapter = new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_dropdown_item_1line,
                            approvedSaleCategories);
                    approvedSaleCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    filterSpinner.setAdapter(approvedSaleCategoryArrayAdapter);
                }

                String msg;
                try {
                    msg = approvedSalesObjAll.getString("message");
                } catch (JSONException e) {
//                    e.printStackTrace();

                    msg = "";   //no msg handling
                }

                if (msg.equals("Invalid token")) {
                    AlertDialog.Builder checkoutAlert = new android.support.v7.app
                            .AlertDialog.Builder(ApproveSaleActivity.this);
                    checkoutAlert.setIcon(R.drawable.ic_error_outline);
                    checkoutAlert.setTitle("Your session has expired");
                    checkoutAlert.setMessage("Please log in again.");
                    checkoutAlert.setCancelable(false);
                    checkoutAlert.setNeutralButton("Close", new DialogInterface
                            .OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            final SharedPreferences.Editor editor = pref.edit();
                            //reset shared pref excluding fcm token
                            editor.putBoolean("isLoggedIn", false);
                            editor.putString("token", "");
                            editor.putString("programId", "");
                            editor.putString("cartCount", "");
                            editor.putString("profile_pic_url", "");
                            editor.putString("first_name", "");
                            editor.putString("last_name", "");
                            editor.putString("cartCount", "0");

                            editor.apply();
                            startActivity(new Intent(ApproveSaleActivity.this, LoginActivity
                                    .class));
                            finish();
                        }
                    });
                    AlertDialog alert = checkoutAlert.create();
                    alert.show();
                } */
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("rootId", root_id);      //eng or sub dealer
                return params;
            }
        };

        salesContactRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(salesContactRequest);
    }

    @Override
    public boolean onLongClick(View v) {
//        approvedSaleBottomBar.getMenu().clear();
//        approvedSaleBottomBar.inflateMenu(R.menu.action_mode_menu);

        approvedSaleBottomBarTitle.setVisibility(View.GONE);
        counterTextView.setText("0 item selected");
        counterTextView.setVisibility(View.VISIBLE);

        is_in_action_mode = true;
        adapter.notifyDataSetChanged();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    public void prepareSelection(View view, int position) {
        if (((CheckBox) view).isChecked()) {
            selection_list.add(approvedSales.get(position));
            count = count + 1;

            updateCounter(count);
        } else {
            selection_list.remove(approvedSales.get(position));
            count = count - 1;
            updateCounter(count);
        }
    }

    private void updateCounter(int counter) {
        if (counter == 0) {
            counterTextView.setText("0 item selected");
        } else {
            counterTextView.setText(counter + " item selected");
        }
    }

    @Override
    public void onBackPressed() {
        if (is_in_action_mode) {
            clearActionMode();
            adapter.notifyDataSetChanged();
        } else {
            super.onBackPressed();
        }
    }

    private void clearActionMode() {
        is_in_action_mode = false;
        approvedSaleBottomBar.getMenu().clear();
        approvedSaleBottomBar.inflateMenu(R.menu.filter_menu);
        counterTextView.setVisibility(View.GONE);
        counterTextView.setText("0 item selected");
        approvedSaleBottomBarTitle.setVisibility(View.VISIBLE);
        count = 0;
        selection_list.clear();
    }

    private void populateSaleDataList(JSONArray saleDataArr) {
        int saleDataLength = saleDataArr.length();

        if (saleDataLength > 0) {
            approveBtnContainer.setVisibility(View.VISIBLE);

            for (int i = 0; i < saleDataLength; i++) {
                try {
                    approvedSales.add(new ApprovedSales(saleDataArr.getJSONObject(i).getString
                            ("Contact_Name"), saleDataArr.getJSONObject(i).getString("ID"), saleDataArr
                            .getJSONObject(i).getString("Product"), saleDataArr.getJSONObject(i).getString
                            ("Bags_Sold"), saleDataArr.getJSONObject(i).getString("unit_name"), saleDataArr
                            .getJSONObject(i).getString("Sale_Registered_On"), saleDataArr.getJSONObject
                            (i).getString("sale_id"), saleDataArr.getJSONObject(i).getString("contact_id")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            adapter = new ApprovedSalesAdapter(approvedSales, ApproveSaleActivity.this);
            approvedSaleRecyclerView.setAdapter(adapter);
        } else {
            approveBtnContainer.setVisibility(View.GONE);
            loadingMin.setVisibility(View.GONE);
            Toast.makeText(ApproveSaleActivity.this, "No sales data found for the selected category!", Toast
                    .LENGTH_SHORT).show();
        }
    }
}