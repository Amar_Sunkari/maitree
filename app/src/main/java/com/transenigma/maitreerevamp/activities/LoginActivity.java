package com.transenigma.maitreerevamp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private AppCompatButton loginBtn;
    private EditText userNameEditText, passwdEditText;

    //UAT credentials
    static final String APIkey = "332827c90c411790ffa33170c13daa8efb12fd4c";
    static final String orgId = "3";
    static final String returnType = "json";
    private static final String contactHierId = "13";
    private String userName = "";
    private String passwd = "";
    private final String os_type = "android";
    private final String os_version = Build.VERSION.RELEASE;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private RelativeLayout loading;
    private final String auth_url = "http://uat-mandres.straightline.in/maitree_req/auth";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userNameEditText = (EditText) findViewById(R.id.input_username);
        passwdEditText = (EditText) findViewById(R.id.input_password);

        loginBtn = (AppCompatButton) findViewById(R.id.login_btn);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0);
        // 0 - for private mode
        editor = pref.edit();

        loading = (RelativeLayout) findViewById(R.id.loadingPanel);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loading.setVisibility(View.VISIBLE);
                userName = userNameEditText.getText().toString().trim();
                passwd = passwdEditText.getText().toString().trim();

                if (userName.equals("")) {
                    Toast.makeText(LoginActivity.this, "User Name is required!", Toast
                            .LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                } else if (passwd.equals("")) {
                    Toast.makeText(LoginActivity.this, "Password is required!", Toast
                            .LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                } else {
                    loginCheck();
                }
            }
        });
    }

    private void loginCheck() {
        StringRequest loginRequest = new StringRequest(Request.Method.POST,
                auth_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);

                JSONObject loginObj = null;
                int bstatus = 0;
                try {
                    loginObj = new JSONObject(response);
                    bstatus = loginObj.getInt("bstatus");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (bstatus == 1) {
                    editor.putBoolean("isLoggedIn", true);
                    try {
                        editor.putString("token", loginObj.getString("token"));
                        editor.putString("programId", loginObj.getString
                                ("currentlySelectedProgram"));
                        editor.putString("first_name", loginObj.getString
                                ("first_name"));
                        editor.putString("last_name", loginObj.getString("last_name"));
                        editor.putString("user_code", loginObj.getString("userCode"));
                        editor.putString("user_tier", loginObj.getString("tier"));
                        editor.putString("profile_pic_url", loginObj.getString
                                ("BaseUrl") + loginObj.getString("profile_pic"));
                        editor.putString("cartCount", loginObj.getString
                                ("total_count"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    editor.commit();
                    loading.setVisibility(View.GONE);
                    startActivity(new Intent(LoginActivity.this, DashActivity
                            .class));
                    finish();
                } else {
                    loading.setVisibility(View.GONE);
                    Toast loginErrorToast = null;
                    try {
                        loginErrorToast = Toast.makeText(LoginActivity.this, "" +
                                loginObj.getString("message"), Toast.LENGTH_LONG);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    loginErrorToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    loginErrorToast.show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("APIkey", APIkey);
                params.put("orgId", orgId);
                params.put("userName", userName);
                params.put("passwd", passwd);
                params.put("contactHierId", contactHierId);
                params.put("os_type", os_type);
                params.put("os_version", os_version);
                params.put("returnType", returnType);

                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                (loginRequest);
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context
                .CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public void forgotPassword(View view) {

        startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));

    }
}