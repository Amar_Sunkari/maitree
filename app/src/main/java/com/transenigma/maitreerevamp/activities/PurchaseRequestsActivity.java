package com.transenigma.maitreerevamp.activities;

import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.PurchaseRequestsAdapter;
import com.transenigma.maitreerevamp.models.Helper;
import com.transenigma.maitreerevamp.models.PurchaseRequest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PurchaseRequestsActivity extends AppCompatActivity {
    @BindView(R.id.activity_dash) public DrawerLayout drawerLayout;
    @BindView(R.id.tool_bar) public Toolbar toolbar;
    @BindView(R.id.navigation_view) public NavigationView navigationView;
    @BindView(R.id.prchase_requests_recyclerview) public RecyclerView purchase_request_recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PurchaseRequestsAdapter purchase_requests_Adapter;
    private final ArrayList<PurchaseRequest> requests = new ArrayList<>();

    private ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_requests);
        ButterKnife.bind(this);


        actionBarDrawerToggle = Helper.setupActivity(PurchaseRequestsActivity.this,actionBarDrawerToggle,drawerLayout,toolbar,"Purchase Requests",navigationView);


        layoutManager= new LinearLayoutManager(getBaseContext());
        purchase_request_recyclerView.setLayoutManager(layoutManager);

        requests.add(new PurchaseRequest("Dinesh",3000f,"Pending", "30th Apr, 2017"));
        requests.add(new PurchaseRequest("Dinesh",3000f,"Pending", "30th Apr, 2017"));
        requests.add(new PurchaseRequest("Dinesh",3000f,"Pending", "30th Apr, 2017"));

        purchase_requests_Adapter = new PurchaseRequestsAdapter(requests, this);


        purchase_request_recyclerView.setAdapter(purchase_requests_Adapter);

    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }
}
