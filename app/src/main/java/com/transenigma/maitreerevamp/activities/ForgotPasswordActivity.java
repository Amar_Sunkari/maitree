package com.transenigma.maitreerevamp.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.orgId;

public class ForgotPasswordActivity extends AppCompatActivity {

    private static final String TAG = ForgotPasswordActivity.class.getSimpleName();
    private final String forgot_password_url = "http://uat-mandres.straightline.in/maitree_req/forgot_password";
    //    protected String forgot_password_programId = "{'0':\"10\",'1':\"11\"}";
    private RelativeLayout loading;
    private SharedPreferences pref;
    private EditText forgetPasswordUserName;
    private AppCompatButton forgetPasswordBtn;
    private Toast errorToast;
    private String userName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        forgetPasswordUserName = (EditText) findViewById(R.id.forget_password_username);
        forgetPasswordBtn = (AppCompatButton) findViewById(R.id.forget_password_btn);

        loading = (RelativeLayout) findViewById(R.id.loadingPanel);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        forgetPasswordBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = forgetPasswordUserName.getText().toString();

                errorToast = new Toast(ForgotPasswordActivity.this);
                errorToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);

                if (userName.trim().equals("")) {
                    errorToast.makeText(getBaseContext(), "Username is required!", Toast
                            .LENGTH_LONG).show();
                } else {
                    loading.setVisibility(View.VISIBLE);
                    forgetPassword(userName.trim());
                }
            }
        });
    }

    private void forgetPassword(final String userName) {

//        Log.i(TAG, "forgetPassword: APIkey = " + APIkey + " programId = " +
//                forgot_password_programId + " orgId =" + orgId + "user_name" + userName);

        final StringRequest forgotPasswordRequest = new StringRequest(Request.Method.POST,
                forgot_password_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.i(TAG, "onResponse: " + response);

                JSONObject forgetPasswordObj;
                int bstatus;
                try {
                    forgetPasswordObj = new JSONObject(response);
                    bstatus = forgetPasswordObj.getInt("bstatus");

                    String msg = forgetPasswordObj.isNull("message") ? "" : forgetPasswordObj
                            .getString("message");
                    if (bstatus == 1) {
                        if (!msg.equals("")) {
                            errorToast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
                            finish();
                        }
                        loading.setVisibility(View.GONE);
                    } else {
                        errorToast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
//                        finish();

//                        errorToast.makeText(getBaseContext(), "Something went wrong! Please try " +
//                                "again.", Toast.LENGTH_LONG).show();
                        loading.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    loading.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("APIkey", APIkey);
                params.put("programId", "7");
                params.put("orgId", orgId);
                params.put("user_name", userName);

                return params;
            }
        };

        forgotPasswordRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                (forgotPasswordRequest);
    }
}