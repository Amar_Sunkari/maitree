package com.transenigma.maitreerevamp.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.SubDealerPerformanceAdapter;
import com.transenigma.maitreerevamp.models.SubDealerPerformance;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class SubDealerPerformanceActivity extends AppCompatActivity {

    private static final String TAG = SubDealerPerformance.class.getSimpleName();
    @BindView(R.id.bottombar)
    Toolbar subDealerPerBottomBar;
    @BindView(R.id.bottombar_title)
    TextView subDealerPerBottomBarTitle;

    @BindView(R.id.sub_dealer_performance_recyclerView)
    RecyclerView subDealerPerfRecyclerVIew;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;
    @BindView(R.id.startDate)
    TextView startDate;
    @BindView(R.id.endDate)
    TextView endDate;

    @BindView(R.id.applyFilterBtn)
    AppCompatButton filterBtn;

    private int year, month, day;

    private SharedPreferences pref;
    private RecyclerView.LayoutManager layoutManager;

    private SubDealerPerformanceAdapter subDealerPerformanceAdapter;
    private final ArrayList<SubDealerPerformance> subDealerPerformance_list = new ArrayList<>();

    private final String sub_dealer_hierarchy_ids = "38,39";
    private final String sub_dealer_performance_url = "http://uat-mandres.straightline.in/maitree_req/subdealer_performance";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_dealer_performance);

        ButterKnife.bind(this);

        setSupportActionBar(subDealerPerBottomBar);
        subDealerPerBottomBarTitle.setText("Sub Dealer Performance");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        layoutManager = new LinearLayoutManager(this);
        subDealerPerfRecyclerVIew.setLayoutManager(layoutManager);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode


        final Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog startDatePickerDialog = new DatePickerDialog(SubDealerPerformanceActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        startDate.setText(i + "-" + (i1 + 1) + "-" + i2);
                    }
                }, year, month, day);
                startDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                startDatePickerDialog.show();
            }
        });

        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog endDatePickerDialog = new DatePickerDialog(SubDealerPerformanceActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        endDate.setText(i + "-" + (i1 + 1) + "-" + i2);
                    }
                }, year, month, day);
                endDatePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
                endDatePickerDialog.show();
            }
        });

        filterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String startDateFinal = startDate.getText().toString();
                String endDateFinal = endDate.getText().toString();

                if (!dateValidation(startDateFinal, endDateFinal)) {
                    Toast.makeText(SubDealerPerformanceActivity.this, "End date must be after Start date.", Toast.LENGTH_SHORT).show();
                } else {
                    loading.setVisibility(View.VISIBLE);
                    subDealerPerformance_list.clear();
                    subDealerPerformanceAdapter.notifyDataSetChanged();
                    getPerformanceList();
                }
            }
        });


        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle(R.string.NETWORK_ERROR_TITLE);
            networkAlert.setMessage(R.string.NETWORK_ERROR_MSG);
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {
            loading.setVisibility(View.VISIBLE);
            getPerformanceList();

        }
    }

    private static boolean dateValidation(String startDate, String endDate) {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date endingDate = df.parse(endDate);
            Date startingDate = df.parse(startDate);

            return endingDate.after(startingDate) || endingDate.equals(startingDate);
        } catch (Exception e) {
            return false;
        }
    }


    private void getPerformanceList() {
        StringRequest subDealerPerfRequest = new StringRequest(Request.Method.POST,
                sub_dealer_performance_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);

                JSONObject subDealerPerfResObj;
                JSONArray subDealerPerfArr;
                try {
                    subDealerPerfResObj = new JSONObject(response);

                    int bstatus = subDealerPerfResObj.getInt("bstatus");

                    if (bstatus == 1) {
                        subDealerPerfArr = subDealerPerfResObj.getJSONArray("children_rec");
                        int subDealerPerfArrLength = subDealerPerfArr.length();

                        for (int i = 0; i < subDealerPerfArrLength; i++) {

                            JSONObject subDealerObj;
                            subDealerObj = subDealerPerfArr.getJSONObject(i);

                            subDealerPerformance_list.add(new SubDealerPerformance(subDealerObj.getString
                                    ("child_f_name"), subDealerObj.getString("id_extern01"), subDealerObj
                                    .getString("total_sale"), subDealerObj.getString("sale_status"), subDealerObj
                                    .getString("child_id"), subDealerObj.getString("child_hierarchies_id"),
                                    subDealerObj.getString("child_is_verified")));
                        }

                        subDealerPerformanceAdapter = new SubDealerPerformanceAdapter(subDealerPerformance_list, getBaseContext());

                        subDealerPerfRecyclerVIew.setAdapter(subDealerPerformanceAdapter);

                        loading.setVisibility(View.GONE);
                    } else {
                        JSONObject notificationMsgObj;
                        String msg = null;
                        try {
                            notificationMsgObj = new JSONObject(response);
                            msg = notificationMsgObj.getString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();

                            loading.setVisibility(View.GONE);
                        }

                        loading.setVisibility(View.GONE);

                        if (msg.equals("Invalid token")) {
                            AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                    .AlertDialog.Builder(SubDealerPerformanceActivity.this);
                            checkoutAlert.setIcon(R.drawable.ic_error_outline);
                            checkoutAlert.setTitle("Your session has expired");
                            checkoutAlert.setMessage("Please log in again.");
                            checkoutAlert.setCancelable(false);
                            checkoutAlert.setNeutralButton("Close", new DialogInterface
                                    .OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    final SharedPreferences.Editor editor = pref.edit();
                                    //reset shared pref excluding fcm token
                                    editor.putBoolean("isLoggedIn", false);
                                    editor.putString("token", "");
                                    editor.putString("programId", "");
                                    editor.putString("cartCount", "");
                                    editor.putString("profile_pic_url", "");
                                    editor.putString("first_name", "");
                                    editor.putString("last_name", "");
                                    editor.putString("cartCount", "0");

                                    editor.apply();
                                    startActivity(new Intent(SubDealerPerformanceActivity.this, LoginActivity
                                            .class));
                                    finish();
                                }
                            });
                            AlertDialog alert = checkoutAlert.create();
                            alert.show();
                        }
                    }
                } catch (JSONException error) {
                    error.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("sub_dealer_hierarchy_ids", sub_dealer_hierarchy_ids);
                params.put("created_from", "");
                params.put("created_to", "");
                params.put("pageNumber", "1");
                params.put("returnType", returnType);
                return params;
            }
        };

        subDealerPerfRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                (subDealerPerfRequest);
    }
}