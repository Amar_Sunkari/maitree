package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.CheckoutAdapter;
import com.transenigma.maitreerevamp.models.CheckoutProduct;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;

public class CheckoutActivity extends AppCompatActivity {

    private static final String TAG = CheckoutActivity.class.getSimpleName();
    private RelativeLayout loadingMin;
    private final ArrayList<CheckoutProduct> checkoutProducts_list = new ArrayList<>();
    private SharedPreferences pref;
    private final String place_order_url = "http://uat-mandres.straightline.in/milan_req/order";
    private Toolbar CheckoutBottomBar;
    private TextView CheckoutBottomBarTitle, totalPtsUsed;
    private AppCompatButton placeOrderBtn;
    private String cartId;
    private RecyclerView checkoutRecyclerView;
    private RecyclerView.LayoutManager checkoutLayoutManager;
    private CheckoutAdapter checkoutAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        CheckoutBottomBar = (Toolbar) findViewById(R.id.bottombar);
        CheckoutBottomBarTitle = (TextView) findViewById(R.id.bottombar_title);

        setSupportActionBar(CheckoutBottomBar);
        CheckoutBottomBarTitle.setText(R.string.CHECKOUT_ACT_TITLE);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_mat);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode
        final SharedPreferences.Editor editor = pref.edit();

        totalPtsUsed = (TextView) findViewById(R.id.total_points);
        placeOrderBtn = (AppCompatButton) findViewById(R.id.place_order_btn);

        loadingMin = (RelativeLayout) findViewById(R.id.loadingPanelMin);

        checkoutRecyclerView = (RecyclerView) findViewById(R.id.checkout_recyclerView);
        checkoutLayoutManager = new LinearLayoutManager(this);
        checkoutRecyclerView.setLayoutManager(checkoutLayoutManager);

        ArrayList<String> prodImg = getIntent().getStringArrayListExtra("prod_img");
        ArrayList<String> prodName = getIntent().getStringArrayListExtra("prod_name");
        ArrayList<String> prodCode = getIntent().getStringArrayListExtra("prod_code");
        ArrayList<String> prodQty = getIntent().getStringArrayListExtra("prod_qty");
        ArrayList<String> prodPts = getIntent().getStringArrayListExtra("prod_pts");


        Log.i(TAG, "onCreate: " + getIntent().getStringArrayListExtra("prod_img") + " " + getIntent().getStringArrayListExtra("prod_name") + " " + getIntent().getStringArrayListExtra("prod_qty") + " " + getIntent().getStringArrayListExtra("prod_pts"));

        cartId = getIntent().getStringExtra("cart_id");

        final JSONObject prodCodeObj = new JSONObject();
        final JSONObject prodQtyObj = new JSONObject();
        final JSONObject prodPtsObj = new JSONObject();

        final JSONObject order_detailsObj = new JSONObject();

        for (int i = 0; i < prodQty.size(); i++) {
            try {
                prodQtyObj.put("" + i, prodQty.get(i));
                prodCodeObj.put("" + i, prodCode.get(i));
                prodPtsObj.put("" + i, prodPts.get(i));

                Log.i(TAG, "onCreate: " + prodName.get(i));
                checkoutProducts_list.add(new CheckoutProduct(prodImg.get(i), prodName.get(i),
                        prodQty.get(i),
                        prodPts.get(i)));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.i(TAG, "onCreate: list" + checkoutProducts_list.toString());

        checkoutAdapter = new CheckoutAdapter(checkoutProducts_list, CheckoutActivity.this);

        checkoutRecyclerView.setAdapter(checkoutAdapter);

        try {
            order_detailsObj.put("productId", prodCodeObj);
            order_detailsObj.put("pricePoints", prodPtsObj);
            order_detailsObj.put("quantity", prodQtyObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        totalPtsUsed.append(getIntent().getStringExtra("cart_total") + " PTS");

        placeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadingMin.setVisibility(View.VISIBLE);
                StringRequest placeOrderRequest = new StringRequest(Request.Method.POST,
                        place_order_url, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i("ORDER", "onResponse: " + response);

                        try {
                            JSONObject orderObj = new JSONObject(response);

                            int bstatus = orderObj.getInt("bstatus");

                            if (bstatus == 1) {
                                editor.putString("cartCount", "0");
                                editor.apply();

                                loadingMin.setVisibility(View.GONE);

                                AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                        .AlertDialog.Builder(CheckoutActivity.this);
                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                checkoutAlert.setTitle("Order Placed");
                                checkoutAlert.setMessage("Your order has been successfully " +
                                        "placed, you can track your order at My Orders");
                                checkoutAlert.setCancelable(false);
                                checkoutAlert.setNeutralButton("Close", new DialogInterface
                                        .OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        finish();
                                    }
                                });
                                AlertDialog alert = checkoutAlert.create();
                                alert.show();
                                supportInvalidateOptionsMenu();
                            } else {
                                loadingMin.setVisibility(View.GONE);

                                String msg = null;
                                try {
                                    msg = orderObj.getString("message");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (msg.equals("Invalid token")) {
                                    AlertDialog.Builder checkoutAlert = new android.support
                                            .v7.app.AlertDialog.Builder(CheckoutActivity.this);
                                    checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                    checkoutAlert.setTitle("Your session has expired");
                                    checkoutAlert.setMessage("Please log in again.");
                                    checkoutAlert.setCancelable(false);
                                    checkoutAlert.setNeutralButton("Close", new DialogInterface
                                            .OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int
                                                i) {
                                            final SharedPreferences.Editor editor = pref.edit();
                                            //reset shared pref excluding fcm token
                                            editor.putBoolean("isLoggedIn", false);
                                            editor.putString("token", "");
                                            editor.putString("programId", "");
                                            editor.putString("cartCount", "");
                                            editor.putString("profile_pic_url", "");
                                            editor.putString("first_name", "");
                                            editor.putString("last_name", "");

                                            editor.apply();
                                            startActivity(new Intent(CheckoutActivity.this,
                                                    LoginActivity.class));
                                            finish();
                                        }
                                    });
                                    AlertDialog alert = checkoutAlert.create();
                                    alert.show();

                                } else {
                                    Toast.makeText(CheckoutActivity.this, "" + msg, Toast
                                            .LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loadingMin.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("token", pref.getString("token", ""));
                        params.put("APIkey", APIkey);
                        params.put("programId", pref.getString("programId", ""));
                        params.put("orderType", "");
                        params.put("productId", "");
                        params.put("pricePoints", "");
                        params.put("quantity", "");
                        params.put("cartId", cartId);
                        params.put("orderDetails", order_detailsObj.toString());
                        return params;
                    }
                };

                placeOrderRequest.setRetryPolicy(new DefaultRetryPolicy(
                        5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                        (placeOrderRequest);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!pref.getBoolean("isLoggedIn", false)) {
            finish();
        }
    }

}