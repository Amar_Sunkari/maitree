package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.Helper;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;

public class DashActivity extends AppCompatActivity {

    private static final String TAG = DashActivity.class.getSimpleName();
    @BindView(R.id.activity_dash)
    public DrawerLayout drawerLayout;
    @BindView(R.id.tool_bar)
    public Toolbar toolbar;
    @BindView(R.id.navigation_view)
    public NavigationView navigationView;

    private ImageView navProfileImg;
    private TextView navUserName, navUserCode, navTier, navAvailablePts;
    static TextView cart_count_tv;

    private ActionBarDrawerToggle actionBarDrawerToggle;

    @BindView(R.id.my_profile)
    FrameLayout myProfile;
    @BindView(R.id.my_rewards)
    FrameLayout myRewards;
    @BindView(R.id.my_orders)
    FrameLayout myOrders;
    @BindView(R.id.news_update)
    FrameLayout newsUpdate;
    @BindView(R.id.about_us)
    FrameLayout aboutUs;
    @BindView(R.id.leaderboard)
    FrameLayout leaderBoard;
    @BindView(R.id.approved_sales)
    FrameLayout approvedSales;
    @BindView(R.id.sub_dealer_performance)
    FrameLayout subDealerPerformance;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private final String extra_data_url = "http://uat-mandres.straightline" +
            ".in/maitree_req/get_dashboard_info";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash);
        ButterKnife.bind(this);

        actionBarDrawerToggle = Helper.setupActivity(DashActivity.this, actionBarDrawerToggle,
                drawerLayout, toolbar, "Maitree", navigationView);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode
        editor = pref.edit();

        //get the nav header view, image view and
        View nav_header_view = navigationView.getHeaderView(0);
        navProfileImg = (ImageView) nav_header_view.findViewById(R.id.nav_profile_image);
        navUserName = (TextView) nav_header_view.findViewById(R.id.nav_user_name_tv);
        navUserCode = (TextView) nav_header_view.findViewById(R.id.nav_user_code_tv);
        navTier = (TextView) nav_header_view.findViewById(R.id.nav_tier_tv);
        navAvailablePts = (TextView) nav_header_view.findViewById(R.id.nav_available_points);

        if (!pref.getString("profile_pic_url", "").equals("")) {
            Picasso.with(getBaseContext()).load(pref.getString("profile_pic_url", "")).into(navProfileImg);
        }

        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle("Network Error");
            networkAlert.setMessage("Please check your internet connection and try again...");
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {
            getExtraData();
        }

        navUserName.setText(pref.getString("first_name", "") + " " + pref.getString("last_name", ""));
        navUserCode.setText(pref.getString("user_code", ""));
        navTier.setText(pref.getString("user_tier", ""));

        Log.i(TAG, "onCreate: " + pref.getString("user_tier", ""));

        myProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, MyProfileActivity.class));
            }
        });

        myRewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, MyRewardsActivity.class));
            }
        });

        newsUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, NewsActivity.class));
            }
        });

        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, AboutUsActivity.class));
            }
        });

        myOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, MyOrdersActivity.class));
            }
        });

        leaderBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, LeaderBoardActivity.class));
            }
        });

        approvedSales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, ApproveSaleActivity.class));
            }
        });

        subDealerPerformance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, SubDealerPerformanceActivity.class));
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!pref.getBoolean("isLoggedIn", false)) {
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Confirm Exit!")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        DashActivity.super.onBackPressed();
                    }
                }).create().show();
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.cart_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cart_popup_menu:
                startActivity(new Intent(getBaseContext(), CartActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem actionViewItem = menu.findItem(R.id.cart_popup_menu);
        View v = MenuItemCompat.getActionView(actionViewItem);
        ImageView cart_iv = (ImageView) v.findViewById(R.id.cart_img);
        cart_count_tv = (TextView) v.findViewById(R.id.cartCountTextView);

        int cart_count = Integer.parseInt((pref.getString("cartCount", "0").equals("")) ? "0" : pref.getString("cartCount", "0"));

        if (cart_count > 0) {
            cart_count_tv.setText(" " + cart_count + " ");
        } else {
            cart_count_tv.setText("0");
        }

        cart_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashActivity.this, CartActivity.class));
            }
        });

        return super.onPrepareOptionsMenu(menu);
    }

    private void getExtraData() {
        StringRequest extraDataRequest = new StringRequest(com.android.volley.Request.Method
                .POST, extra_data_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: extra " + response);

                int bstatus = 0;
                JSONObject extraObj = null;
                try {
                    extraObj = new JSONObject(response);
                    bstatus = extraObj.getInt("bstatus");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (bstatus == 1) {
                    try {
                        navAvailablePts.setText("Points available " + extraObj.getString
                                ("available_point"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                return params;
            }
        };

        extraDataRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        );

        VolleySingleton.getInstance(getApplicationContext()).
                addToRequestQueue(extraDataRequest);
    }


    private void logout() {
        AlertDialog.Builder checkoutAlert = new android.support.v7.app.AlertDialog.Builder
                (DashActivity.this);
        checkoutAlert.setIcon(R.drawable.ic_error_outline);
        checkoutAlert.setTitle("Confirm Logout");
        checkoutAlert.setMessage("Are you sure you want to Logout?");
        checkoutAlert.setCancelable(false);
        checkoutAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                editor.putBoolean("isLoggedIn", false);
                editor.putString("token", "");
                editor.putString("programId", "");
                editor.putString("cartCount", "0");
                editor.putString("profile_pic_url", "");
                editor.putString("first_name", "");
                editor.putString("last_name", "");
                editor.commit();

                Intent intent = new Intent(DashActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent
                        .FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        checkoutAlert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        AlertDialog alert = checkoutAlert.create();
        alert.show();
    }
}