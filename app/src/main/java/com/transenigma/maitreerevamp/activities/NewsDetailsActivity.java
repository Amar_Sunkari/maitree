package com.transenigma.maitreerevamp.activities;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewsDetailsActivity extends AppCompatActivity {

    private WebView notificationDetailsWebView;
    private SharedPreferences pref;

    @BindView(R.id.bottombar)
    public Toolbar NotificationDetailsBottomBar;
    @BindView(R.id.bottombar_title)
    public TextView NotificationDetailsBottomBarTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(this);

        setSupportActionBar(NotificationDetailsBottomBar);
        NotificationDetailsBottomBarTitle.setText(R.string.ACT_NEWS_UPDATES_TITLE);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_mat);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        notificationDetailsWebView = (WebView) findViewById(R.id.notification_details_webview);

        notificationDetailsWebView.getSettings().getJavaScriptEnabled();
        notificationDetailsWebView.setWebViewClient(new WebViewClient());
        notificationDetailsWebView.setBackgroundColor(Color.TRANSPARENT);
        notificationDetailsWebView.setLayerType(notificationDetailsWebView.LAYER_TYPE_SOFTWARE,
                null);

        String strText = getIntent().getStringExtra("news_body_html");

        strText = "<div style='text-align:center;'>\n" + strText + "</div>";

        notificationDetailsWebView.loadDataWithBaseURL(null, strText, "text/html; charset=utf-8",
                "UTF-8", null);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!pref.getBoolean("isLoggedIn", false)) {
            finish();
        }
    }

}