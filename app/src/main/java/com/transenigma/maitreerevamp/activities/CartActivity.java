package com.transenigma.maitreerevamp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.CartProductAdapter;
import com.transenigma.maitreerevamp.models.CartProduct;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;

public class CartActivity extends AppCompatActivity {

    private static final String TAG = CartActivity.class.getSimpleName();

    @BindView(R.id.bottombar)
    Toolbar cartBottomBar;
    @BindView(R.id.bottombar_title)
    TextView cartBottomBarTitle;

    @BindView(R.id.cart_recylerview)
    public RecyclerView cartRecyclerView;
    @BindView(R.id.checkout_button)
    public Button checkoutBtn;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;

    private RecyclerView.LayoutManager cartLayoutManager;
    private final ArrayList<CartProduct> cartProducts_list = new ArrayList<>();
    private CartProductAdapter cartProductAdapter;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private ProgressDialog pDialog;
    private String cartTotal = "";
    private String cartId = "";

    private final String my_cart_url = "http://uat-mandres.straightline.in/maitree_req/mycart";
    private final String cart_item_update_url = "http://uat-mandres.straightline.in/maitree_req/save_quantity";
    private final String cart_item_remove_url = "http://uat-mandres.straightline.in/maitree_req/removecart";

    private final ArrayList<String> checkout_prod_img_list = new ArrayList<>();
    private final ArrayList<String> checkout_prod_list = new ArrayList<>();
    private final ArrayList<String> checkout_code_list = new ArrayList<>();
    private final ArrayList<String> checkout_qty_list = new ArrayList<>();
    private final ArrayList<String> checkout_pts_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        setSupportActionBar(cartBottomBar);
        cartBottomBarTitle.setText("My Cart");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cartLayoutManager = new LinearLayoutManager(CartActivity.this);
        cartRecyclerView.setLayoutManager(cartLayoutManager);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode
        editor = pref.edit();

        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle(R.string.NETWORK_ERROR_TITLE);
            networkAlert.setMessage(R.string.NETWORK_ERROR_MSG);
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();

        } else {
            pDialog = new ProgressDialog(CartActivity.this);
            StringRequest myCartRequest = new StringRequest(Request.Method.POST, my_cart_url, new
                    Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i("MY CART", "onResponse: " + response);
                            if (!response.isEmpty()) {
                                try {
                                    JSONObject cartObj = new JSONObject(response);

                                    int bstatus = cartObj.getInt("bstatus");
                                    if (bstatus == 1) {
                                        JSONObject cartQtyObj = cartObj.getJSONObject("control");
                                        JSONObject cartProdObj = cartObj.getJSONObject("row_items");
                                        cartTotal = cartQtyObj.getString("grandtotal_in_point");
                                        Iterator<String> iter = cartProdObj.keys();
                                        editor.putString("cartCount", cartQtyObj.getString
                                                ("items"));
                                        editor.apply();

                                        checkout_prod_img_list.clear();
                                        checkout_prod_list.clear();
                                        checkout_code_list.clear();
                                        checkout_qty_list.clear();
                                        checkout_pts_list.clear();
                                        while (iter.hasNext()) {
                                            String key = iter.next();
                                            try {
                                                Object order = cartProdObj.get(key);
                                                JSONObject prodObj = new JSONObject(order
                                                        .toString());
                                                CartProduct cartProduct = new CartProduct(prodObj
                                                        .getString("BaseUrl") + prodObj.getString
                                                        ("product_image"), prodObj.getString
                                                        ("product_id"), prodObj.getString
                                                        ("product_name")
                                                        , prodObj.getString("quantity"), prodObj
                                                        .getString("price_in_points"), prodObj
                                                        .getString
                                                                ("cart_id"));

                                                checkout_prod_img_list.add(prodObj
                                                        .getString("BaseUrl") + prodObj.getString
                                                        ("product_image"));
                                                checkout_prod_list.add(prodObj.getString
                                                        ("product_name"));
                                                checkout_code_list.add(prodObj.getString
                                                        ("product_id"));
                                                checkout_qty_list.add(prodObj.getString
                                                        ("quantity"));
                                                checkout_pts_list.add(prodObj.getString
                                                        ("price_in_points"));

                                                cartProducts_list.add(cartProduct);

                                                cartId = prodObj.getString("cart_id");

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    } else {
                                        String msg = null;
                                        try {
                                            msg = cartObj.isNull("message") ? null : cartObj
                                                    .getString("message");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        if (msg.equals("Invalid token")) {
                                            AlertDialog.Builder checkoutAlert = new android.support
                                                    .v7.app.AlertDialog.Builder(CartActivity.this);
                                            checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                            checkoutAlert.setTitle("Your session has expired");
                                            checkoutAlert.setMessage("Please log in again.");
                                            checkoutAlert.setCancelable(false);
                                            checkoutAlert.setNeutralButton("Close", new
                                                    DialogInterface
                                                            .OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface
                                                                                    dialogInterface, int
                                                                                    i) {
                                                            final SharedPreferences.Editor editor
                                                                    = pref
                                                                    .edit();
                                                            //reset shared pref excluding fcm token
                                                            editor.putBoolean("isLoggedIn", false);
                                                            editor.putString("token", "");
                                                            editor.putString("programId", "");
                                                            editor.putString("cartCount", "");
                                                            editor.putString("profile_pic_url", "");
                                                            editor.putString("first_name", "");
                                                            editor.putString("last_name", "");
                                                            editor.putString("cartCount", "0");

                                                            editor.apply();
                                                            startActivity(new Intent(CartActivity
                                                                    .this,
                                                                    LoginActivity.class));
                                                            finish();
                                                        }
                                                    });
                                            AlertDialog alert = checkoutAlert.create();
                                            alert.show();

                                        } else if (msg.equals("Cart is empty")) {
                                            editor.putString("cartCount", "0");
                                            editor.commit();
                                            loading.setVisibility(View.GONE);

                                            Toast.makeText(CartActivity.this, "Empty Cart!", Toast
                                                    .LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(CartActivity.this, "Something went " +
                                                            "wrong",
                                                    Toast.LENGTH_SHORT).show();

                                            loading.setVisibility(View.GONE);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                                cartRecyclerView.setLayoutManager(cartLayoutManager);
                                cartProductAdapter = new CartProductAdapter(cartProducts_list,
                                        CartActivity.this, new CartProductAdapter.ClickHandler() {
                                    @Override
                                    public void onMyButtonClicked(int position, int qty, int mode) {
                                        final CartProduct cartProdClicked = cartProductAdapter
                                                .getItem(position);

                                        if (mode == 0) {
                                            AlertDialog.Builder checkoutAlert = new android.support
                                                    .v7.app.AlertDialog.Builder(CartActivity.this);
                                            checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                            checkoutAlert.setTitle("Remove Product");
                                            checkoutAlert.setMessage("Remove product from Cart?");
                                            checkoutAlert.setCancelable(false);
                                            checkoutAlert.setPositiveButton("Yes", new
                                                    DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface
                                                                                    dialogInterface, int
                                                                                    i) {
                                                            removeCartItem(cartProdClicked
                                                                            .getCart_id(),
                                                                    cartProdClicked.getProd_id());
                                                            //Reload activity
                                                            finish();
                                                            startActivity(getIntent());
                                                        }
                                                    });
                                            checkoutAlert.setNegativeButton("No", new
                                                    DialogInterface
                                                            .OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface
                                                                                    dialogInterface, int
                                                                                    i) {
                                                        }
                                                    });
                                            AlertDialog alert = checkoutAlert.create();
                                            alert.show();

                                        } else if (mode == 1) {
                                            int qtyIncreased = qty + 1;
                                            updateCartItem(cartProdClicked.getCart_id(), "" +
                                                    qtyIncreased, cartProdClicked.getProd_id());

                                            Toast.makeText(CartActivity.this, "Item Quantity " +
                                                            "Updated",
                                                    Toast.LENGTH_SHORT).show();
                                            //Reload activity
                                            finish();
                                            startActivity(getIntent());
                                        } else {
                                            int qtyDecreased = qty - 1;
                                            updateCartItem(cartProdClicked.getCart_id(), "" +
                                                    qtyDecreased, cartProdClicked.getProd_id());
                                            Toast.makeText(CartActivity.this, "Item Quantity " +
                                                            "Updated",
                                                    Toast.LENGTH_SHORT).show();
                                            //Reload activity
                                            finish();
                                            startActivity(getIntent());
                                        }
                                    }
                                });

                                cartRecyclerView.setAdapter(cartProductAdapter);

                                if (!cartProducts_list.isEmpty()) {
                                    checkoutBtn.setVisibility(View.VISIBLE);
                                }

//                        pDialog.dismiss();
                                loading.setVisibility(View.GONE);
                            } else {
                                loading.setVisibility(View.GONE);
                                Toast.makeText(CartActivity.this, "Empty Cart!", Toast
                                        .LENGTH_SHORT).show();
                            }
                        }
                    }

                    , new Response.ErrorListener()

            {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    pDialog.setMessage("Network Error! Please Try again.");
                    pDialog.show();
                }
            }

            )

            {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("token", pref.getString("token", ""));
                    params.put("APIkey", APIkey);
                    params.put("programId", "4");
                    return params;
                }
            };

            myCartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            VolleySingleton.getInstance(getApplicationContext()).
                    addToRequestQueue(myCartRequest);
        }
//
//        cartProducts_list.add(new CartProduct("http://www.lafarge.in/wps/wcm/connect/88510000447b25908b92fb10dbe33715/concreto-bag190.jpg?MOD=AJPERES&CACHEID=88510000447b25908b92fb10dbe33715","192","Concreto", "108","16108","16"));
//        cartProducts_list.add(new CartProduct("http://www.lafarge.in/wps/wcm/connect/88510000447b25908b92fb10dbe33715/concreto-bag190.jpg?MOD=AJPERES&CACHEID=88510000447b25908b92fb10dbe33715","192","DuraGuard", "108","16108","16"));
//        cartProducts_list.add(new CartProduct("http://www.lafarge.in/wps/wcm/connect/88510000447b25908b92fb10dbe33715/concreto-bag190.jpg?MOD=AJPERES&CACHEID=88510000447b25908b92fb10dbe33715","192","PSC", "108","16108","16"));
//        cartProducts_list.add(new CartProduct("http://www.lafarge.in/wps/wcm/connect/88510000447b25908b92fb10dbe33715/concreto-bag190.jpg?MOD=AJPERES&CACHEID=88510000447b25908b92fb10dbe33715","192","Dura Build", "108","16108","16"));

//        cartProductAdapter = new CartProductAdapter(cartProducts_list, CartActivity.this);
//
//        cartRecyclerView.setAdapter(cartProductAdapter);

        checkoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent checkoutIntent = new Intent(getBaseContext(), CheckoutActivity.class);
                checkoutIntent.putStringArrayListExtra("prod_img", checkout_prod_img_list);
                checkoutIntent.putStringArrayListExtra("prod_name", checkout_prod_list);
                checkoutIntent.putStringArrayListExtra("prod_code", checkout_code_list);
                checkoutIntent.putStringArrayListExtra("prod_qty", checkout_qty_list);
                checkoutIntent.putStringArrayListExtra("prod_pts", checkout_pts_list);
                checkoutIntent.putExtra("cart_id", cartId);
                checkoutIntent.putExtra("cart_total", cartTotal);

                startActivity(checkoutIntent);
            }
        });
    }

    private void updateCartItem(final String cart_id, final String quantity, final String prod_id) {
        StringRequest updateCartItemRequest = new StringRequest(Request.Method.POST,
                cart_item_update_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("REMOVE", "onResponse: " + response);
                cartProductAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("programId", pref.getString("programId", ""));
                params.put("cart_id", cart_id);
                params.put("quantity", quantity);
                params.put("product_id", prod_id);
                return params;
            }
        };

        updateCartItemRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                (updateCartItemRequest);
    }

    private void removeCartItem(final String cart_id, final String prod_id) {
        StringRequest removeCartItemRequest = new StringRequest(Request.Method.POST,
                cart_item_remove_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "onResponse: " + response);
                Toast.makeText(CartActivity.this, "Item Removed", Toast.LENGTH_SHORT).show();
                cartProductAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("programId", "");
                params.put("cart_id", cart_id);
                params.put("product_id", prod_id);
                return params;
            }
        };

        removeCartItemRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                (removeCartItemRequest);
    }
}