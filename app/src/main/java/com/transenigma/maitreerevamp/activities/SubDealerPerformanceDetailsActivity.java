package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.SubDealerPerformanceDetailsAdapter;
import com.transenigma.maitreerevamp.models.SubDealerTransactions;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class SubDealerPerformanceDetailsActivity extends AppCompatActivity {

    private static final String TAG = SubDealerPerformanceDetailsActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    Toolbar subDealerPerfDetailsBottomBar;

    @BindView(R.id.bottombar_title)
    TextView subDealerPerfDetailsBottomBarTitle;

    @BindView(R.id.sub_dealer_performance_details_recyclerView)
    RecyclerView subDealerPerfDetailsRecyclerView;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;

    private RecyclerView.LayoutManager layoutManager;

    private final ArrayList<SubDealerTransactions> subDealerTransactions_list = new ArrayList<>();

    private SharedPreferences pref;

    private final String sub_dealer_perf_details_url = "http://uat-mandres.straightline.in/maitree_req/sub_dealer_transaction_details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_dealer_performance_details);

        ButterKnife.bind(this);

        setSupportActionBar(subDealerPerfDetailsBottomBar);
        subDealerPerfDetailsBottomBarTitle.setText("Performance Details");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        layoutManager = new LinearLayoutManager(this);

        subDealerPerfDetailsRecyclerView.setLayoutManager(layoutManager);

        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle(R.string.NETWORK_ERROR_TITLE);
            networkAlert.setMessage(R.string.NETWORK_ERROR_MSG);
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {

            loading.setVisibility(View.VISIBLE);
            StringRequest subDealerPerfDetailsRequest = new StringRequest(Request.Method.POST,
                    sub_dealer_perf_details_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i(TAG, "onResponse: " + response);

                    JSONObject subDealerPerfDetailsResObj;
                    JSONArray subDealerPerfDetailsArr;
                    try {
                        subDealerPerfDetailsResObj = new JSONObject(response);

                        int bstatus = subDealerPerfDetailsResObj.getInt("bstatus");

                        if (bstatus == 1) {
                            subDealerPerfDetailsArr = subDealerPerfDetailsResObj.getJSONArray("sale_details");
                            int subDealerPerfDetailsArrLength = subDealerPerfDetailsArr.length();

                            for (int i = 0; i < subDealerPerfDetailsArrLength; i++) {

                                JSONObject subDealerTransObj;
                                subDealerTransObj = subDealerPerfDetailsArr.getJSONObject(i);

                                subDealerTransactions_list.add(new SubDealerTransactions(subDealerTransObj.getString
                                        ("sale_id"), subDealerTransObj.getString("Product"), subDealerTransObj
                                        .getString("Bags_Sold"), subDealerTransObj.getString("Sale_Date"),
                                        subDealerTransObj.getString("Product_Purchased_From"), subDealerTransObj
                                        .getString("Dealer_Code"), subDealerTransObj.getString("Sale_Registered_On"), subDealerTransObj.getString("sale_id")));
                            }

                            SubDealerPerformanceDetailsAdapter subDealerPerformanceDetailsAdapter = new
                                    SubDealerPerformanceDetailsAdapter(subDealerTransactions_list, getBaseContext());

                            subDealerPerfDetailsRecyclerView.setAdapter(subDealerPerformanceDetailsAdapter);

                            loading.setVisibility(View.GONE);
                        } else {
                            JSONObject notificationMsgObj;
                            String msg = null;
                            try {
                                notificationMsgObj = new JSONObject(response);
                                msg = notificationMsgObj.getString("message");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            loading.setVisibility(View.GONE);

                            if (msg.equals("Invalid token")) {
                                AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                        .AlertDialog.Builder(SubDealerPerformanceDetailsActivity.this);
                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                checkoutAlert.setTitle("Your session has expired");
                                checkoutAlert.setMessage("Please log in again.");
                                checkoutAlert.setCancelable(false);
                                checkoutAlert.setNeutralButton("Close", new DialogInterface
                                        .OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final SharedPreferences.Editor editor = pref.edit();
                                        //reset shared pref excluding fcm token
                                        editor.putBoolean("isLoggedIn", false);
                                        editor.putString("token", "");
                                        editor.putString("programId", "");
                                        editor.putString("cartCount", "");
                                        editor.putString("profile_pic_url", "");
                                        editor.putString("first_name", "");
                                        editor.putString("last_name", "");
                                        editor.putString("cartCount", "0");

                                        editor.apply();
                                        startActivity(new Intent(SubDealerPerformanceDetailsActivity.this, LoginActivity
                                                .class));
                                        finish();
                                    }
                                });
                                AlertDialog alert = checkoutAlert.create();
                                alert.show();
                            }
                        }
                    } catch (JSONException error) {
                        error.printStackTrace();
                        loading.setVisibility(View.GONE);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("token", pref.getString("token", ""));
                    params.put("APIkey", APIkey);
                    params.put("child_contact_id", getIntent().getStringExtra("child_contact_id"));
                    params.put("child_hierarchies_id", getIntent().getStringExtra("child_hierarchies_id"));
                    params.put("is_verified", getIntent().getStringExtra("child_is_verified"));
                    params.put("startDate", "");
                    params.put("endDate", "");
                    params.put("pageNumber", "1");
                    params.put("returnType", returnType);
                    return params;
                }
            };

            subDealerPerfDetailsRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                    (subDealerPerfDetailsRequest);
        }
    }
}
