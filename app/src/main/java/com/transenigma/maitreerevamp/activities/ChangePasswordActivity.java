package com.transenigma.maitreerevamp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class ChangePasswordActivity extends AppCompatActivity {

    @BindView(R.id.bottombar)
    Toolbar changePasswordToolBar;

    @BindView(R.id.bottombar_title)
    TextView changePasswordBottomBarTitle;

    @BindView(R.id.loadingPanel)
    public RelativeLayout loading;
    @BindView(R.id.tv_status_msg)
    public TextView status_msg;
    @BindView(R.id.et_update_pass_current)
    public TextView current_pass;
    @BindView(R.id.et_update_pass_new)
    public TextView new_pass;
    @BindView(R.id.et_update_pass_new_repeat)
    public TextView new_pass_repeat;

    @BindView(R.id.btn_update_pass)
    public AppCompatButton update_pass_btn;

    private final String change_password_url = "http://uat-mandres.straightline" +
            ".in/milan_req/changepassword";

    private String current_pass_string;
    private String new_pass_string;
    private String new_pass_repeat_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        ButterKnife.bind(this);

        setSupportActionBar(changePasswordToolBar);
        changePasswordBottomBarTitle.setText("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        final SharedPreferences pref = getApplicationContext().getSharedPreferences
                ("UserInitPref", 0); // 0 - for private mode

        if (!isNetworkConnected(getBaseContext())) {

            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_warning_black_24dp);
            networkAlert.setTitle("Network Error");
            networkAlert.setMessage("Please check your internet connection and try again...");
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {
            final ProgressDialog pDialog = new ProgressDialog(ChangePasswordActivity.this);

            update_pass_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    current_pass_string = current_pass.getText().toString().trim();
                    new_pass_string = new_pass.getText().toString().trim();
                    new_pass_repeat_string = new_pass_repeat.getText().toString().trim();

                    if (current_pass_string.equals("")) {
                        status_msg.setText("Current password is required!");
                        if (status_msg.getVisibility() != View.VISIBLE) {
                            status_msg.setVisibility(View.VISIBLE);
                        }
                    } else if (new_pass_string.equals("")) {
                        status_msg.setText("New password is required!");
                        if (status_msg.getVisibility() != View.VISIBLE) {
                            status_msg.setVisibility(View.VISIBLE);
                        }
                    } else if (new_pass_repeat_string.equals("")) {
                        status_msg.setText("Repeat new password is required!");
                        if (status_msg.getVisibility() != View.VISIBLE) {
                            status_msg.setVisibility(View.VISIBLE);
                        }
                    } else {
                        status_msg.setVisibility(View.GONE);

                        loading.setVisibility(View.VISIBLE);

                        final StringRequest updatePassRequest = new StringRequest(Request.Method
                                .POST, change_password_url, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.i("Pass", response);

                                JSONObject updatePassObj = null;
                                int bstatus = 0;
                                try {
                                    updatePassObj = new JSONObject(response);

                                    bstatus = updatePassObj.getInt("bstatus");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                if (bstatus == 1) {
                                    try {
                                        status_msg.setText(updatePassObj.getString("message"));
                                        status_msg.setVisibility(View.VISIBLE);

                                        startActivity(new Intent(ChangePasswordActivity.this,
                                                DashActivity.class));
                                        finish();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    loading.setVisibility(View.GONE);
                                } else {
                                    loading.setVisibility(View.GONE);
                                    String msg = null;
                                    try {
                                        msg = updatePassObj.getString("message");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    if (msg.equals("Invalid token")) {
                                        AlertDialog.Builder checkoutAlert = new android.support
                                                .v7.app.AlertDialog.Builder
                                                (ChangePasswordActivity.this);
                                        checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                        checkoutAlert.setTitle("Your session has expired");
                                        checkoutAlert.setMessage("Please log in again.");
                                        checkoutAlert.setCancelable(false);
                                        checkoutAlert.setNeutralButton("Close", new
                                                DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface
                                                                                dialogInterface,
                                                                        int i) {
                                                        final SharedPreferences.Editor editor =
                                                                pref.edit();
                                                        //reset shared pref excluding fcm token
                                                        editor.putBoolean("isLoggedIn", false);
                                                        editor.putString("token", "");
                                                        editor.putString("programId", "");
                                                        editor.putString("cartCount", "");
                                                        editor.putString("profile_pic_url", "");
                                                        editor.putString("first_name", "");
                                                        editor.putString("last_name", "");
                                                        editor.putString("cartCount", "0");

                                                        editor.apply();
                                                        Intent intent = new Intent
                                                                (ChangePasswordActivity
                                                                        .this, LoginActivity.class);
                                                        intent.addFlags(Intent
                                                                .FLAG_ACTIVITY_CLEAR_TOP |
                                                                Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
                                        AlertDialog alert = checkoutAlert.create();
                                        alert.show();
                                    } else {
                                        Toast.makeText(ChangePasswordActivity.this, msg, Toast
                                                .LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                pDialog.setMessage(getString(R.string.VOLLEY_ERROR_MSG));
                                pDialog.show();
                                loading.setVisibility(View.GONE);
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String, String> params = new HashMap<>();
                                params.put("token", pref.getString("token", ""));
                                params.put("APIkey", APIkey);
                                params.put("old_password", current_pass_string);
                                params.put("new_password", new_pass_string);
                                params.put("confirm_password", new_pass_repeat_string);
                                params.put("programId", pref.getString("programId", ""));
                                params.put("returnType", returnType);

                                return params;
                            }
                        };

                        updatePassRequest.setRetryPolicy(new DefaultRetryPolicy(
                                5000,
                                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
                        );

                        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                                (updatePassRequest);
                    }
                }
            });
        }
    }
}
