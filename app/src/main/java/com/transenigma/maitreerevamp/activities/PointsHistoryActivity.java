package com.transenigma.maitreerevamp.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;

public class PointsHistoryActivity extends AppCompatActivity {

    private static final String TAG = PointsHistoryActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    Toolbar pointsHistoryBottomBar;
    @BindView(R.id.bottombar_title)
    TextView pointsHistoryBottomBarTitle;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;

    private final String points_history_url = "http://uat-mandres.straightline.in/maitree_req/pointstatements";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_points_history);

        ButterKnife.bind(this);
        setSupportActionBar(pointsHistoryBottomBar);
        pointsHistoryBottomBarTitle.setText("Points History");

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode
        editor = pref.edit();


        if (!isNetworkConnected(getBaseContext())) {

            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_warning_black_24dp);
            networkAlert.setTitle("Network Error");
            networkAlert.setMessage("Please check your internet connection and try again...");
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {

            final ProgressDialog pDialog = new ProgressDialog(PointsHistoryActivity.this);

            StringRequest pointsHistoryRequest = new StringRequest(Request.Method.POST,
                    points_history_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i(TAG, response);

                    populatePointsHistory(response);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    pDialog.setMessage("Network Error! Please Try again.");
                    pDialog.show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("token", pref.getString("token", ""));
                    params.put("APIkey", APIkey);
                    params.put("programId", pref.getString("programId", ""));
                    params.put("returnType", "json");
//                params.put("from_date","2016-10-01");
//                params.put("till_date","2016-11-30");
                    return params;
                }
            };
            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                    (pointsHistoryRequest);
        }
    }

    private void populatePointsHistory(final String response) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    JSONObject pointsObj = new JSONObject(response);

                    int bstatus = 0;
                    try {
                        bstatus = pointsObj.getInt("bstatus");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (bstatus == 1) {
                        JSONArray transArray = pointsObj.isNull("trnasc_list") ? null : pointsObj
                                .getJSONArray("trnasc_list");

                        if (transArray != null) {
                            TableLayout pointsHistoryTabLayout = (TableLayout) findViewById(R.id
                                    .points_history_layout_table);

                            TableRow.LayoutParams trlp = new TableRow.LayoutParams(0, TableRow
                                    .LayoutParams.MATCH_PARENT, 0.33f);

                            for (int i = 0; i < transArray.length(); i++) {

                                JSONObject tObj = transArray.getJSONObject(i);
                                TableRow tbrow = new TableRow(PointsHistoryActivity.this);

                                TextView t1v = new TextView(PointsHistoryActivity.this);
                                t1v.setText(tObj.getString("transaction_type"));
                                t1v.setGravity(Gravity.LEFT);
                                t1v.setLayoutParams(trlp);
                                t1v.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                        .divider, null));
                                tbrow.addView(t1v);

                                TextView t2v = new TextView(PointsHistoryActivity.this);
                                t2v.setText(tObj.getString("reward_points"));
                                t2v.setGravity(Gravity.CENTER);
                                t2v.setLayoutParams(trlp);
                                t2v.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                        .divider, null));
                                tbrow.addView(t2v);

                                TextView t3v = new TextView(PointsHistoryActivity.this);
                                t3v.setText(tObj.getString("created_at"));
                                t3v.setGravity(Gravity.RIGHT);
                                t3v.setLayoutParams(trlp);
                                t3v.setTextColor(ResourcesCompat.getColor(getResources(), R.color
                                        .divider, null));
                                tbrow.addView(t3v);

                                pointsHistoryTabLayout.addView(tbrow);

                                loading.setVisibility(View.GONE);
                            }
                        } else {
                            Toast.makeText(PointsHistoryActivity.this, "No Transaction found!",
                                    Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                        }
                    } else {
                        String msg = pointsObj.getString("message");
                        if (msg.equals("Invalid token")) {
                            AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                    .AlertDialog.Builder(PointsHistoryActivity.this);
                            checkoutAlert.setIcon(R.drawable.ic_error_outline);
                            checkoutAlert.setTitle("Your session has expired");
                            checkoutAlert.setMessage("Please log in again.");
                            checkoutAlert.setCancelable(false);
                            checkoutAlert.setNeutralButton("Close", new DialogInterface
                                    .OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //reset shared pref excluding fcm token
                                    editor.putBoolean("isLoggedIn", false);
                                    editor.putString("token", "");
                                    editor.putString("programId", "");
                                    editor.putString("cartCount", "");
                                    editor.putString("profile_pic_url", "");
                                    editor.putString("first_name", "");
                                    editor.putString("last_name", "");
                                    editor.putString("cartCount", "0");

                                    editor.commit();

                                    startActivity(new Intent(PointsHistoryActivity.this,
                                            LoginActivity.class));
                                    finish();
                                }
                            });
                            AlertDialog alert = checkoutAlert.create();
                            alert.show();

                        } else {
                            Toast.makeText(PointsHistoryActivity.this, "Something went wrong",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
//                    e.printStackTrace();
                    loading.setVisibility(View.GONE);
                    Toast.makeText(PointsHistoryActivity.this, "Something went wrong! Please try " +
                            "again...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}