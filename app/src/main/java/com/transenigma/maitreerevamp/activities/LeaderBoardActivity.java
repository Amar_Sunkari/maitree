package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.LeaderBoardPagerAdapter;
import com.transenigma.maitreerevamp.fragments.MyPositionFragment;
import com.transenigma.maitreerevamp.fragments.TopTenFragment;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class LeaderBoardActivity extends AppCompatActivity {

    private static final String TAG = LeaderBoardActivity.class.getSimpleName();
    @BindView(R.id.bottombar)
    public Toolbar leaderBoardBottomBar;
    @BindView(R.id.bottombar_title)
    TextView leaderBoardBottomBarTitle;

    @BindView(R.id.loadingPanel)
    RelativeLayout loading;

//    @BindView(R.id.leader_board_recyclerview)
//    public RecyclerView leader_board_recyclerView;
//    public RecyclerView.LayoutManager layoutManager;
//    public LeaderBoardAdapter leader_board_Adapter;
//    public ArrayList<DealerDetails> dealers = new ArrayList<>();

    private TabLayout leaderBoardTabLayout;
    private ViewPager leaderBoardViewPager;
    private LeaderBoardPagerAdapter leaderBoardPagerAdapter;

    private SharedPreferences pref;

    private final String leader_board_url = "http://uat-mandres.straightline.in/maitree_req/leaderboard";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        ButterKnife.bind(this);

        setSupportActionBar(leaderBoardBottomBar);

        leaderBoardBottomBarTitle.setText("LeaderBoard");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0); // 0 - for
        // private mode

        leaderBoardTabLayout = (TabLayout) findViewById(R.id.leaderBoard_tabLayout);
        leaderBoardViewPager = (ViewPager) findViewById(R.id.leaderBoard_viewPager);

        leaderBoardTabLayout.setupWithViewPager(leaderBoardViewPager);

        leaderBoardPagerAdapter = new LeaderBoardPagerAdapter(getSupportFragmentManager());

        /*
        openLeadsTabLayout = (TabLayout) findViewById(R.id.open_leads_tablayout);
        openLeadsViewPager = (ViewPager) findViewById(R.id.open_leads_viewPager);

        openLeadsTabLayout.setupWithViewPager(openLeadsViewPager);
        openLeadsPagerAdapter = new OpenLeadsPagerAdapter(getSupportFragmentManager());
        */


/*
        openLeadsPagerAdapter.addFragments(openLeadsFragment);
        openLeadsPagerAdapter.addFragments(onGoingProjectsFragment);
        openLeadsViewPager.setAdapter(openLeadsPagerAdapter);
*/

        if (!isNetworkConnected(getBaseContext())) {
            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_error_outline);
            networkAlert.setTitle(R.string.NETWORK_ERROR_TITLE);
            networkAlert.setMessage(R.string.NETWORK_ERROR_MSG);
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {
            loading.setVisibility(View.VISIBLE);

            StringRequest leaderBoardRequest = new StringRequest(Request.Method.POST,
                    leader_board_url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.i(TAG, "onResponse: " + response);

                    JSONObject leaderBoardResObj;

                    try {
                        leaderBoardResObj = new JSONObject(response);

                        int bstatus = leaderBoardResObj.getInt("bstatus");

                        if (bstatus == 1) {
                            JSONObject topTenObj = leaderBoardResObj.getJSONObject("topTen");
                            JSONObject myPositionObj = leaderBoardResObj.getJSONObject("myPosition");
                            Log.i(TAG, "onResponse: Top " + topTenObj.toString() + " My Pos" + myPositionObj);

                            TopTenFragment openLeadsFragment = new TopTenFragment().newInstance(topTenObj.toString());

                            MyPositionFragment myPositionFragment = new MyPositionFragment()
                                    .newInstance(myPositionObj.toString());

                            leaderBoardPagerAdapter.addFragments(openLeadsFragment);
                            leaderBoardPagerAdapter.addFragments(myPositionFragment);

                            leaderBoardViewPager.setAdapter(leaderBoardPagerAdapter);

                            loading.setVisibility(View.GONE);
                        } else {
                            String msg = leaderBoardResObj.getString("message");

                            loading.setVisibility(View.GONE);

                            if (msg.equals("Invalid token")) {
                                AlertDialog.Builder checkoutAlert = new android.support.v7.app
                                        .AlertDialog.Builder(LeaderBoardActivity.this);
                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                checkoutAlert.setTitle("Your session has expired");
                                checkoutAlert.setMessage("Please log in again.");
                                checkoutAlert.setCancelable(false);
                                checkoutAlert.setNeutralButton("Close", new DialogInterface
                                        .OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        final SharedPreferences.Editor editor = pref.edit();
                                        //reset shared pref excluding fcm token
                                        editor.putBoolean("isLoggedIn", false);
                                        editor.putString("token", "");
                                        editor.putString("programId", "");
                                        editor.putString("cartCount", "");
                                        editor.putString("profile_pic_url", "");
                                        editor.putString("first_name", "");
                                        editor.putString("last_name", "");
                                        editor.putString("cartCount", "0");

                                        editor.apply();
                                        startActivity(new Intent(LeaderBoardActivity.this, LoginActivity
                                                .class));
                                        finish();
                                    }
                                });
                                AlertDialog alert = checkoutAlert.create();
                                alert.show();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("token", pref.getString("token", ""));
                    params.put("APIkey", APIkey);
                    params.put("programId", pref.getString("programId", ""));
                    params.put("contentCode", "lafargeMaitreeLeaderboard");
                    params.put("returnType", returnType);
                    return params;
                }
            };

            leaderBoardRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue
                    (leaderBoardRequest);
        }

        /*
        dealers.add(new DealerDetails("Mr. White", "https://encrypted-tbn1.gstatic" +
                ".com/images?q=tbn:ANd9GcRdtRABuU0zsmhmwk5yDk12j377kfSF0zEITgX7weX4trst8VTfcQ", "3450000", "1"));
        dealers.add(new DealerDetails("Mr. White", "https://encrypted-tbn1.gstatic" +
                ".com/images?q=tbn:ANd9GcRdtRABuU0zsmhmwk5yDk12j377kfSF0zEITgX7weX4trst8VTfcQ", "345000", "2"));
        dealers.add(new DealerDetails("Mr. White", "https://encrypted-tbn1.gstatic" +
                ".com/images?q=tbn:ANd9GcRdtRABuU0zsmhmwk5yDk12j377kfSF0zEITgX7weX4trst8VTfcQ", "50000", "3"));

        leader_board_Adapter = new LeaderBoardAdapter(dealers, this);
        leader_board_recyclerView.setAdapter(leader_board_Adapter);
        */
    }
}
