package com.transenigma.maitreerevamp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.PurchaseRequestsDetailsAdapter;
import com.transenigma.maitreerevamp.models.PurchaseRequestProduct;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PurchaseRequestDetailsActivity extends AppCompatActivity {

    @BindView(R.id.requestor)TextView requestor;
    @BindView(R.id.request_status)TextView request_status;
    @BindView(R.id.purchase_amount)TextView purchase_amount;
    @BindView(R.id.tool_bar)Toolbar toolbar;
    @BindView(R.id.purchase_requests_details_recyclerview) public RecyclerView purchase_request_details_recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PurchaseRequestsDetailsAdapter purchase_requests_details_Adapter;
    private final ArrayList<PurchaseRequestProduct> products= new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_request_details);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Purchase Request");



        requestor.setText(getIntent().getStringExtra("requestor_name"));
        request_status.setText(getIntent().getStringExtra("request_status"));
        purchase_amount.setText(getIntent().getStringExtra("purchase_amount"));

        layoutManager= new LinearLayoutManager(getBaseContext());
        purchase_request_details_recyclerView.setLayoutManager(layoutManager);

        products.add(new PurchaseRequestProduct("Concrete 20kg",25,25000f));
        products.add(new PurchaseRequestProduct("DuraGuard 25kg",25,30000f));
        products.add(new PurchaseRequestProduct("InfraCem 35kg",40,40000f));



        purchase_requests_details_Adapter = new PurchaseRequestsDetailsAdapter(products, this);


        purchase_request_details_recyclerView.setAdapter(purchase_requests_details_Adapter);



    }
}
