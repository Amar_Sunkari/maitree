package com.transenigma.maitreerevamp.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.adapters.MyOrderAdapter;
import com.transenigma.maitreerevamp.models.MyOrder;
import com.transenigma.maitreerevamp.models.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.transenigma.maitreerevamp.activities.LoginActivity.APIkey;
import static com.transenigma.maitreerevamp.activities.LoginActivity.isNetworkConnected;
import static com.transenigma.maitreerevamp.activities.LoginActivity.orgId;
import static com.transenigma.maitreerevamp.activities.LoginActivity.returnType;

public class MyOrdersActivity extends AppCompatActivity {

    private static final String TAG = MyOrdersActivity.class.getSimpleName();

    @BindView(R.id.bottombar)
    public Toolbar myOrdersBottomBar;
    @BindView(R.id.bottombar_title)
    TextView myOrdersBottomBarTitle;

    private RelativeLayout loading;

    private RecyclerView myOrders_recycler_view;
    private MyOrderAdapter myOrders_adapter;
    private RecyclerView.LayoutManager myOrders_layout_manager;

    private SharedPreferences pref;

    private final ArrayList<MyOrder> myOrders_list = new ArrayList<>();

    private final String my_orders_url = "http://uat-mandres.straightline.in/maitree_req/myorders";
    private final String my_order_cancel_url = "http://uat-mandres.straightline" +
            ".in/maitree_req/cancel_order";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        ButterKnife.bind(this);

        setSupportActionBar(myOrdersBottomBar);
        myOrdersBottomBarTitle.setText("My Order");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        loading = (RelativeLayout) findViewById(R.id.loadingPanel);

        // 0 - for private mode
        pref = getApplicationContext().getSharedPreferences("UserInitPref", 0);

        if (!isNetworkConnected(getBaseContext())) {

            AlertDialog.Builder networkAlert = new android.support.v7.app.AlertDialog.Builder(this);
            networkAlert.setIcon(R.drawable.ic_warning_black_24dp);
            networkAlert.setTitle("Network Error");
            networkAlert.setMessage("Please check your internet connection and try again...");
            networkAlert.setCancelable(false);
            networkAlert.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            AlertDialog alert = networkAlert.create();
            alert.show();
        } else {

            StringRequest myOrdersRequest = new StringRequest(Request.Method.POST, my_orders_url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.i(TAG, "onResponse: " + response);

                            JSONObject myOrderResObject = null;
                            int bstatus = 0;
                            try {
                                myOrderResObject = new JSONObject(response);
                                bstatus = myOrderResObject.getInt("bstatus");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (bstatus == 1) {
                                try {
                                    JSONArray myOrdersArr = myOrderResObject.getJSONArray("orders");

                                    int myOrdersArrLength = myOrdersArr.length();
                                    if (myOrdersArrLength > 0) {

                                        for (int i = 0; i < myOrdersArrLength; i++) {
                                            try {
                                                JSONObject myOrderObj = myOrdersArr
                                                        .getJSONObject(i);

                                                String prod_img = "";
                                                if (!myOrderObj.getString("product_image").equals("null")) {
                                                    prod_img = myOrderObj.getString
                                                            ("BaseUrl") + myOrderObj.getString
                                                            ("product_image");
                                                }

                                                MyOrder myOrder = new MyOrder(prod_img,
                                                        myOrderObj.getString("productName"),
                                                        myOrderObj.getString("productCode"),
                                                        myOrderObj.getString("orderItemId"),
                                                        myOrderObj.getString("pricePoint"),
                                                        myOrderObj.getString("status"),
                                                        myOrderObj.getString("orderId"),
                                                        myOrderObj.getString("orderDate"),
                                                        myOrderObj.getString("awbNo"),
                                                        myOrderObj.getString("courierName"),
                                                        myOrderObj.getString("canCancel"));

                                                myOrders_list.add(myOrder);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        myOrders_recycler_view = (RecyclerView) findViewById(R.id
                                                .myorders_recyclerview);
                                        myOrders_layout_manager = new LinearLayoutManager
                                                (MyOrdersActivity.this);
                                        myOrders_recycler_view.setLayoutManager
                                                (myOrders_layout_manager);

                                        myOrders_adapter = new MyOrderAdapter(myOrders_list,
                                                MyOrdersActivity.this, new MyOrderAdapter.OrderClickHandler() {
                                            @Override
                                            public void onCancelBtnClicked(int position) {
                                                final MyOrder order = myOrders_adapter.getItem(position);

                                                //Show confirmation dialog
                                                AlertDialog.Builder checkoutAlert = new android
                                                        .support.v7.app.AlertDialog.Builder(MyOrdersActivity.this);
                                                checkoutAlert.setIcon(R.drawable.ic_error_outline);
                                                checkoutAlert.setTitle("Cancel Order");
                                                checkoutAlert.setMessage("Cancel Product Order?");
                                                checkoutAlert.setCancelable(false);
                                                checkoutAlert.setPositiveButton("Yes", new
                                                        DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface
                                                                                        dialogInterface, int
                                                                                        i) {
                                                                cancelOrderProduct(order);
                                                                //Reload activity
                                                                finish();
                                                                startActivity(getIntent());
                                                            }
                                                        });
                                                checkoutAlert.setNegativeButton("No", new
                                                        DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface
                                                                                        dialogInterface, int
                                                                                        i) {
                                                            }
                                                        });
                                                AlertDialog alert = checkoutAlert.create();
                                                alert.show();
                                            }
                                        });
                                        myOrders_recycler_view.setAdapter(myOrders_adapter);
                                        loading.setVisibility(View.GONE);

                                    } else {
                                        loading.setVisibility(View.GONE);
                                        Toast.makeText(MyOrdersActivity.this, "No order found.", Toast
                                                .LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    loading.setVisibility(View.GONE);
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                    Toast.makeText(MyOrdersActivity.this, R.string.VOLLEY_ERROR_MSG, Toast
                            .LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("APIkey", APIkey);
                    params.put("token", pref.getString("token", ""));
                    params.put("contentCode", "lafargeMaitreeRedeem");
                    params.put("programId", "4");
                    params.put("returnType", returnType);

                    return params;
                }
            };

            myOrdersRequest.setRetryPolicy(new DefaultRetryPolicy(
                    5000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
            );

            VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(myOrdersRequest);
        }
    }

    private void cancelOrderProduct(final MyOrder myOrder) {
        StringRequest orderCancelRequest = new StringRequest(Request.Method.POST,
                my_order_cancel_url, new
                Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "onResponse: cancel" + response);

                        myOrders_adapter.notifyDataSetChanged();
                        JSONObject orderCancelObj = null;
                        int bstatus = 0;
                        try {
                            orderCancelObj = new JSONObject(response);
                            bstatus = orderCancelObj.getInt("bstatus");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String msg = "";
                        if (bstatus == 1) {
                            try {
                                msg = orderCancelObj.isNull("message") ? null : orderCancelObj.getString("message");
                            } catch (JSONException e) {
                                Toast.makeText(MyOrdersActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                            Toast.makeText(MyOrdersActivity.this, "" + msg, Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("token", pref.getString("token", ""));
                params.put("APIkey", APIkey);
                params.put("programId", pref.getString("programId", ""));
                params.put("orgId", orgId);
                params.put("itemId", myOrder.getProduct_item_id());
                params.put("orderId", myOrder.getOrder_id());
                params.put("status", "Cancelled");
                return params;
            }
        };

        orderCancelRequest.setRetryPolicy(new DefaultRetryPolicy(
                20000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        VolleySingleton.getInstance(getApplicationContext()).addToRequestQueue(orderCancelRequest);
    }

}