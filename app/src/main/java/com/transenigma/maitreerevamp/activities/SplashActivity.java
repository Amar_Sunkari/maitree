package com.transenigma.maitreerevamp.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.transenigma.maitreerevamp.R;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //get user info from shared pref
        final SharedPreferences pref = getApplicationContext().getSharedPreferences
                ("UserInitPref", 0); // 0 - for private mode

        Thread myThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);

                    if (pref.getBoolean("isLoggedIn", false)) {
                        Intent intent = new Intent(SplashActivity.this, DashActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        myThread.start();
    }
}
