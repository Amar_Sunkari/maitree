package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.PurchaseRequestProduct;

import java.util.ArrayList;

/**
 * Created by Amar on 06-Mar-17.
 */


public class PurchaseRequestsDetailsAdapter extends RecyclerView.Adapter<PurchaseRequestsDetailsAdapter.PurchaseRequestDetailsViewHolder> {

    private final ArrayList<PurchaseRequestProduct> product_list;
    private final Context context;


    public PurchaseRequestsDetailsAdapter(ArrayList<PurchaseRequestProduct> product_list, Context context) {

        this.product_list = product_list;
        this.context = context;

    }

    @Override
    public PurchaseRequestDetailsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.purchase_request_product_row, parent, false);
        PurchaseRequestDetailsViewHolder requestViewHolder = new PurchaseRequestDetailsViewHolder(view, context, product_list);
        return requestViewHolder;
    }

    @Override
    public void onBindViewHolder(PurchaseRequestDetailsViewHolder holder, int position) {

        PurchaseRequestProduct product = product_list.get(position);

        holder.product_name_textview.setText(product.getProduct_name());
        holder.product_quantity_textview.setText(product.getQuantity() + "");
        holder.product_sub_total_textview.setText("Rs." + product.getSub_total());

    }

    @Override
    public int getItemCount() {
        return product_list.size();
    }

    public PurchaseRequestProduct getItem(int position) {
        return product_list.get(position);
    }

    public static class PurchaseRequestDetailsViewHolder extends RecyclerView.ViewHolder {

        final TextView product_name_textview;
        final TextView product_quantity_textview;
        final TextView product_sub_total_textview;

        ArrayList<PurchaseRequestProduct> product_list = new ArrayList<>();
        final Context context;


        public PurchaseRequestDetailsViewHolder(View itemView, Context context, ArrayList<PurchaseRequestProduct> product_list) {

            super(itemView);
            this.product_list = product_list;
            this.context = context;

            product_name_textview = (TextView) itemView.findViewById(R.id.product_name);
            product_quantity_textview = (TextView) itemView.findViewById(R.id.product_quantity);
            product_sub_total_textview = (TextView) itemView.findViewById(R.id.sub_total);

        }

    }

}
