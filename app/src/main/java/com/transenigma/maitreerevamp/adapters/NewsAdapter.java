package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.NewsDetailsActivity;
import com.transenigma.maitreerevamp.models.News;

import java.util.ArrayList;

/**
 * Created by subhadip on 11/12/16.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsAdapterViewHolder> {

    private ArrayList<News> news_list = new ArrayList<>();
    private final Context context;

    public NewsAdapter(ArrayList<News> news_list, Context context) {
        this.news_list = news_list;
        this.context = context;
    }

    @Override
    public NewsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .notifications_cardview, parent, false);
        NewsAdapterViewHolder NewsAdapterViewHolder = new NewsAdapterViewHolder(view, context,
                news_list);

        return NewsAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(NewsAdapterViewHolder holder, int position) {
        News News = news_list.get(position);

        holder.news_title_textView.setText(News.getNews_title());
        holder.news_date_textView.setText(News.getNews_date());
    }

    @Override
    public int getItemCount() {
        return news_list.size();
    }

    public static class NewsAdapterViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {

        final TextView news_title_textView;
        final TextView news_date_textView;
        ArrayList<News> news_list = new ArrayList<>();
        final Context context;

        public NewsAdapterViewHolder(View itemView, Context context, ArrayList<News>
                news_list) {
            super(itemView);
            this.context = context;
            this.news_list = news_list;

            itemView.setOnClickListener(this);

            news_title_textView = (TextView) itemView.findViewById(R.id.news_title);
            news_date_textView = (TextView) itemView.findViewById(R.id.news_date);
        }

        @Override
        public void onClick(View view) {

            int position = getAdapterPosition();
            News News = this.news_list.get(position);

            Intent News_details_intent = new Intent(this.context,
                    NewsDetailsActivity.class);
            News_details_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            News_details_intent.putExtra("news_body_html", News
                    .getNews_body());

            this.context.startActivity(News_details_intent);
        }
    }
}