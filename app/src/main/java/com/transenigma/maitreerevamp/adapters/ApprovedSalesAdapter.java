package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.ApproveSaleActivity;
import com.transenigma.maitreerevamp.models.ApprovedSales;

import java.util.ArrayList;

/**
 * Created by subhadip on 12/4/17.
 */

public class ApprovedSalesAdapter extends RecyclerView.Adapter<ApprovedSalesAdapter.ApprovedSalesAdapterViewHolder> {

    private final ArrayList<ApprovedSales> approvedSales_list;
    private final ApproveSaleActivity approveSaleActivity;
    private final Context context;

    public ApprovedSalesAdapter(ArrayList<ApprovedSales> approvedSales, Context context) {
        this.approvedSales_list = approvedSales;
        this.approveSaleActivity = (ApproveSaleActivity) context;
        this.context = context;
    }

    @Override
    public ApprovedSalesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_approve_sale, parent, false);

        ApprovedSalesAdapterViewHolder approvedSalesAdapterViewholder = new ApprovedSalesAdapterViewHolder(view, approveSaleActivity);
        return approvedSalesAdapterViewholder;
    }

    @Override
    public void onBindViewHolder(final ApprovedSalesAdapterViewHolder holder, int position) {
        ApprovedSales approvedSales = approvedSales_list.get(position);

        if (!approveSaleActivity.is_in_action_mode) {
            holder.approvedCheckbox.setVisibility(View.GONE);
        } else {
            holder.approvedCheckbox.setVisibility(View.VISIBLE);
            holder.approvedCheckbox.setChecked(false);
        }

        holder.approvedCheckbox.setOnCheckedChangeListener(null);

        try {
            holder.approvedCheckbox.setSelected(approvedSales.getSelected());
        } catch (Exception e) {
//            e.printStackTrace();
        }

        holder.approvedCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                approvedSales_list.get(holder.getAdapterPosition()).setSelected(isChecked);
            }
        });

        holder.prod_name.setText(approvedSales.getProd_name());
        holder.prod_qty.setText(approvedSales.getProd_qty() + " " + approvedSales.getProd_unit());
        holder.prod_sale_regn_date.setText(approvedSales.getProd_regn_date());
        holder.name.setText(approvedSales.getTitle());
        holder.id.setText(approvedSales.getId());
    }

    @Override
    public int getItemCount() {
        return approvedSales_list.size();
    }

    public class ApprovedSalesAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final TextView prod_name;
        final TextView prod_qty;
        final TextView prod_sale_regn_date;
        final TextView name;
        final TextView id;
        final CheckBox approvedCheckbox;
        Context context;
        final ApproveSaleActivity approveSaleActivity;

        public ApprovedSalesAdapterViewHolder(View itemView, ApproveSaleActivity approveSaleActivity) {
            super(itemView);
            this.approveSaleActivity = approveSaleActivity;

            prod_name = (TextView) itemView.findViewById(R.id.apprv_sale_prod_name);
            prod_qty = (TextView) itemView.findViewById(R.id.apprv_sale_qty);
            prod_sale_regn_date = (TextView) itemView.findViewById(R.id.apprv_sale_sale_regn_date);
            name = (TextView) itemView.findViewById(R.id.apprv_sale_name);
            id = (TextView) itemView.findViewById(R.id.apprv_sale_id);
            approvedCheckbox = (CheckBox) itemView.findViewById(R.id.approved_checkbox);

            itemView.setOnLongClickListener(approveSaleActivity);
            approvedCheckbox.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            approveSaleActivity.prepareSelection(v, getAdapterPosition());
        }
    }
}