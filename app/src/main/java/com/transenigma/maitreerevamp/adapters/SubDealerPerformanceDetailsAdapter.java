package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.SubDealerTransactions;

import java.util.ArrayList;

/**
 * Created by subhadip on 6/4/17.
 */

public class SubDealerPerformanceDetailsAdapter extends RecyclerView.Adapter<SubDealerPerformanceDetailsAdapter.SubDealerPerformanceDetailsAdapterViewHolder> {

    private ArrayList<SubDealerTransactions> sub_dealer_transactions_details_list = new ArrayList<>();
    private final Context context;

    public SubDealerPerformanceDetailsAdapter(ArrayList<SubDealerTransactions> sub_dealer_performance_details_list, Context context) {
        this.sub_dealer_transactions_details_list = sub_dealer_performance_details_list;
        this.context = context;
    }

    @Override
    public SubDealerPerformanceDetailsAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_dealer_transactions, parent,
                false);
        SubDealerPerformanceDetailsAdapterViewHolder subDealerPerformanceDetailsAdapterViewHolder = new
                SubDealerPerformanceDetailsAdapterViewHolder(view, context);

        return subDealerPerformanceDetailsAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(SubDealerPerformanceDetailsAdapterViewHolder holder, int position) {

        SubDealerTransactions subDealerTransactions = sub_dealer_transactions_details_list.get(position);
        holder.transactionId.setText(subDealerTransactions.getTransc_id());
        holder.productName.setText(subDealerTransactions.getProduct_name());
        holder.productSold.setText(subDealerTransactions.getProduct_sold());
        holder.saleDate.setText(subDealerTransactions.getSale_date());
        holder.purchasedFrom.setText(subDealerTransactions.getPurchased_from());
        holder.dealerCode.setText(subDealerTransactions.getDealer_code());
        holder.registeredOn.setText(subDealerTransactions.getRegistered_on());

//        holder.status.setText(subDealerTransactions.getStatus());
    }

    @Override
    public int getItemCount() {
        return sub_dealer_transactions_details_list.size();
    }

    public static class SubDealerPerformanceDetailsAdapterViewHolder extends RecyclerView.ViewHolder {

        final TextView transactionId;
        final TextView productName;
        final TextView productSold;
        final TextView saleDate;
        final TextView purchasedFrom;
        final TextView dealerCode;
        final TextView registeredOn;
        final Context context;


        public SubDealerPerformanceDetailsAdapterViewHolder(View itemView, Context context) {
            super(itemView);
            this.context = context;

            transactionId = (TextView) itemView.findViewById(R.id.transc_id_tv);
            productName = (TextView) itemView.findViewById(R.id.prod_name_tv);
            productSold = (TextView) itemView.findViewById(R.id.prod_sold_tv);
            saleDate = (TextView) itemView.findViewById(R.id.sale_date_tv);
            purchasedFrom = (TextView) itemView.findViewById(R.id.purchased_from_tv);
            dealerCode = (TextView) itemView.findViewById(R.id.dealer_code_tv);
            registeredOn = (TextView) itemView.findViewById(R.id.registed_on_tv);
//            status = (TextView) itemView.findViewById(R.id.status_tv);
        }
    }
}
