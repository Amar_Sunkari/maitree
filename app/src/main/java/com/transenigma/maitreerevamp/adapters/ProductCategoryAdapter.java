package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.ProductCategory;

import java.util.ArrayList;

/**
 * Created by subhadip on 18/11/16.
 */

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.CategoryViewHolder> {

    private final OnItemClickListener listener;
    private final ArrayList<ProductCategory> categories;
    private final Context context;

    public ProductCategoryAdapter(ArrayList<ProductCategory> categories, Context context, OnItemClickListener listener) {
        this.categories = categories;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_categories, parent, false);
        CategoryViewHolder categoryViewHolder = new CategoryViewHolder(view);

        return categoryViewHolder;
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, int position) {
        final ProductCategory category = categories.get(position);

        holder.categoryName.setText(category.getName());
        holder.categoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(category);
                holder.categoryName.setTextColor(Color.RED);
            }
        });

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public interface OnItemClickListener {
        void onItemClick(ProductCategory category);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        final TextView categoryName;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            categoryName = (TextView) itemView.findViewById(R.id.category_title);
        }
    }
}