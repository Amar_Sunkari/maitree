package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.ProductDetailsActivity;
import com.transenigma.maitreerevamp.models.MyReward;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

/**
 * Created by subhadip on 6/11/16.
 */

public class MyRewardAdapter extends RecyclerView.Adapter<MyRewardAdapter.MyRewardHolder> {

    final static int FADE_DURATION = 500;
    private static ArrayList<MyReward> myRewards;
    private static Context context;
    private int lastPosition = -1;


    public MyRewardAdapter(ArrayList<MyReward> myRewards, Context context) {
        this.myRewards = myRewards;
        this.context = context;
    }

    @Override
    public MyRewardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_rewards,
                parent, false);
        MyRewardHolder myRewardsHolder = new MyRewardHolder(view);
        return myRewardsHolder;
    }

    @Override
    public void onBindViewHolder(MyRewardHolder holder, int position) {

        MyReward myReward = myRewards.get(position);

        if (!Objects.equals(myReward.getProd_img_url(), "")) {
            Picasso.with(context).load(myReward.getProd_img_url()).into(holder.prod_img_rewards);
        }

        holder.prod_name_rewards.setText(myReward.getProd_name());
        holder.prod_pts_rewards.setText(myReward.getProd_pts());

        setFadeAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return myRewards.size();
    }

    private void setFadeAnimation(View view, int position) {
//        AlphaAnimation anim = new AlphaAnimation(0.0f,1.0f);

        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation
                    .RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between
            // [0,501)
            view.startAnimation(anim);
            lastPosition = position;
        }
    }

    public static class MyRewardHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {

        final ImageView prod_img_rewards;
        final TextView prod_name_rewards;
        final TextView prod_pts_rewards;
        final RecyclerView rewards_recyclerView;

        public MyRewardHolder(View view) {
            super(view);

            rewards_recyclerView = (RecyclerView) view.findViewById(R.id.rewards_recylerview);
            prod_img_rewards = (ImageView) view.findViewById(R.id.cv_prod_img_rewards);
            prod_name_rewards = (TextView) view.findViewById(R.id.cv_prod_name_rewards);
            prod_pts_rewards = (TextView) view.findViewById(R.id.cv_prod_pts_rewards);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

            int product_position = getAdapterPosition();
            MyReward myReward_product = myRewards.get(product_position);

            Intent product_details_intent = new Intent(context, ProductDetailsActivity.class);
            product_details_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            if (!Objects.equals(myReward_product.getProd_img_url(), "")) {
                product_details_intent.putExtra("prod_img", "" + myReward_product.getProd_img_url
                        ());
            } else {
                product_details_intent.putExtra("prod_img", "");
            }

            product_details_intent.putExtra("prod_id", myReward_product.getProd_id());
            product_details_intent.putExtra("prod_name", myReward_product.getProd_name());
            product_details_intent.putExtra("prod_points", myReward_product.getProd_pts());
            product_details_intent.putExtra("prod_params", myReward_product.getProd_params());
            product_details_intent.putExtra("prod_details", myReward_product.getProd_details());
            context.startActivity(product_details_intent);
        }
    }
}
