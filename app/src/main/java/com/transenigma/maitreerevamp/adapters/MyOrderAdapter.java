package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.MyOrderDetailsActivity;
import com.transenigma.maitreerevamp.models.MyOrder;

import java.util.ArrayList;

/**
 * Created by subhadip on 12/11/16.
 */

public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyOrderViewHolder> {

    private final OrderClickHandler orderClickHandler;
    private final ArrayList<MyOrder> myOrders_list;
    private final Context context;

    public MyOrderAdapter(ArrayList<MyOrder> myOrders_list, Context context, OrderClickHandler
            orderClickHandler) {
        this.myOrders_list = myOrders_list;
        this.context = context;
        this.orderClickHandler = orderClickHandler;
    }

    @Override
    public MyOrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_orders,
                parent, false);
        MyOrderViewHolder myOrderHolder = new MyOrderViewHolder(view, context, myOrders_list);
        return myOrderHolder;
    }

    @Override
    public void onBindViewHolder(MyOrderViewHolder holder, int position) {
        MyOrder myOrder = myOrders_list.get(position);

        if (!myOrder.getProduct_image().equals("")) {
            Picasso.with(context).load(myOrder.getProduct_image()).resize(80, 80).into(holder
                    .prod_image);
        }

        holder.tv_order_id.setText(myOrder.getOrder_id());
        holder.tv_prod_name.setText(myOrder.getProduct_name());
        holder.tv_prod_status.setText(myOrder.getStatus());
        holder.tv_order_date.setText(myOrder.getOrder_date());

        if (myOrder.getCan_cancel().equals("1")) {
            if (myOrder.getStatus().equals("Open") || myOrder.getStatus().equals("Lost")) {
                holder.order_cancel_textview.setVisibility(View.VISIBLE);
            } else {
                holder.order_cancel_textview.setVisibility(View.GONE);
            }
        } else {
            holder.order_cancel_textview.setVisibility(View.GONE);
        }
        holder.vHOrderClickHandler = this.orderClickHandler;
    }

    @Override
    public int getItemCount() {
        return myOrders_list.size();
    }

    public MyOrder getItem(int position) {
        return myOrders_list.get(position);
    }

    public interface OrderClickHandler {
        void onCancelBtnClicked(final int position);
    }

    public static class MyOrderViewHolder extends RecyclerView.ViewHolder implements View
            .OnClickListener {

        final AppCompatImageView prod_image;
        final TextView tv_prod_name;
        final TextView tv_prod_status;
        final TextView tv_order_id;
        final TextView tv_order_date;
        final TextView order_cancel_textview;
        ArrayList<MyOrder> myOrders = new ArrayList<>();
        final Context context;

        OrderClickHandler vHOrderClickHandler;

        public MyOrderViewHolder(View itemView, Context context, ArrayList<MyOrder> myOrders) {

            super(itemView);
            this.myOrders = myOrders;
            this.context = context;

            itemView.setOnClickListener(this);

            prod_image = (AppCompatImageView) itemView.findViewById(R.id.order_prod_image);
            tv_prod_name = (TextView) itemView.findViewById(R.id.order_title);
            tv_prod_status = (TextView) itemView.findViewById(R.id.order_status);
            tv_order_id = (TextView) itemView.findViewById(R.id.order_id);
            tv_order_date = (TextView) itemView.findViewById(R.id.order_date);
            order_cancel_textview = (TextView) itemView.findViewById(R.id.cancel_ord_tv);

            order_cancel_textview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (vHOrderClickHandler != null) {
                        vHOrderClickHandler.onCancelBtnClicked(getAdapterPosition());
                    }
                }
            });
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            MyOrder myOrder = this.myOrders.get(position);

            Intent my_order_details_intent = new Intent(this.context, MyOrderDetailsActivity.class);
            my_order_details_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            my_order_details_intent.putExtra("order_image", myOrder.getProduct_image());
            my_order_details_intent.putExtra("order_id", myOrder.getOrder_id());
            my_order_details_intent.putExtra("order_date", myOrder.getOrder_date());
            my_order_details_intent.putExtra("prod_name", myOrder.getProduct_name());
            my_order_details_intent.putExtra("prod_code", myOrder.getProduct_code());
            my_order_details_intent.putExtra("price_point", myOrder.getPrice_point());
            my_order_details_intent.putExtra("awb_no", myOrder.getAwb_no());
            my_order_details_intent.putExtra("courier_name", myOrder.getCourier_name());
            my_order_details_intent.putExtra("prod_status", myOrder.getStatus());
            my_order_details_intent.putExtra("prod_img", myOrder.getProduct_image());

            this.context.startActivity(my_order_details_intent);
        }
    }
}