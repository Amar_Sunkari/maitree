package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.PurchaseRequestDetailsActivity;
import com.transenigma.maitreerevamp.models.PurchaseRequest;

import java.util.ArrayList;

/**
 * Created by Amar on 06-Mar-17.
 */


public class PurchaseRequestsAdapter extends RecyclerView.Adapter<PurchaseRequestsAdapter.PurchaseRequestViewHolder> {

    private final ArrayList<PurchaseRequest> request_list;
    private final Context context;


    public PurchaseRequestsAdapter(ArrayList<PurchaseRequest> request_list, Context context) {

        this.request_list = request_list;
        this.context = context;

    }

    @Override
    public PurchaseRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.puchase_request_card_view, parent, false);
        PurchaseRequestViewHolder requestViewHolder = new PurchaseRequestViewHolder(view, context, request_list);
        return requestViewHolder;
    }

    @Override
    public void onBindViewHolder(PurchaseRequestViewHolder holder, int position) {

        PurchaseRequest request = request_list.get(position);

        holder.requestor_name_textview.setText(request.getRequestor_name());
        holder.request_date_textview.setText(request.getRequest_date());
        holder.request_status_textview.setText(request.getRequest_status());
        holder.purchase_amount_textview.setText("Rs." + request.getPurchase_amount());


    }

    @Override
    public int getItemCount() {
        return request_list.size();
    }

    public PurchaseRequest getItem(int position) {
        return request_list.get(position);
    }

    public static class PurchaseRequestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final TextView requestor_name_textview;
        final TextView purchase_amount_textview;
        final TextView request_date_textview;
        final TextView request_status_textview;

        ArrayList<PurchaseRequest> requests = new ArrayList<>();
        final Context context;


        public PurchaseRequestViewHolder(View itemView, Context context, ArrayList<PurchaseRequest> requests) {

            super(itemView);
            this.requests = requests;
            this.context = context;

            itemView.setOnClickListener(this);

            requestor_name_textview = (TextView) itemView.findViewById(R.id.requestor_name);
            purchase_amount_textview = (TextView) itemView.findViewById(R.id.purchase_amount);
            request_date_textview = (TextView) itemView.findViewById(R.id.request_date);
            request_status_textview = (TextView) itemView.findViewById(R.id.request_status);


        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            PurchaseRequest request = this.requests.get(position);

            Intent purchase_requests_details_intent = new Intent(this.context, PurchaseRequestDetailsActivity.class);
            purchase_requests_details_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            purchase_requests_details_intent.putExtra("requestor_name", request.getRequestor_name());
            purchase_requests_details_intent.putExtra("request_date", request.getRequest_date());
            purchase_requests_details_intent.putExtra("request_status", request.getRequest_status());

            purchase_requests_details_intent.putExtra("purchase_amount", "Rs. " + request.getPurchase_amount());

            this.context.startActivity(purchase_requests_details_intent);
        }
    }


}
