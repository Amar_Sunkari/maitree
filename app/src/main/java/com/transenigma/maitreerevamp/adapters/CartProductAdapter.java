package com.transenigma.maitreerevamp.adapters;

/**
 * Created by Amar on 09-Mar-17.
 */

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.CartProduct;

import java.util.ArrayList;

/**
 * Created by subhadip on 18/12/16.
 */

public class CartProductAdapter extends RecyclerView.Adapter<CartProductAdapter.CartProductViewholder> {

    private final ClickHandler clickHandler;
    private ArrayList<CartProduct> cartProducts_list = new ArrayList<>();
    private final Context context;

    public CartProductAdapter(ArrayList<CartProduct> cartProducts_list, Context context, ClickHandler clickHandler) {
        this.cartProducts_list = cartProducts_list;
        this.context = context;
        this.clickHandler = clickHandler;
    }

    @Override
    public CartProductViewholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_cart, parent, false);
        CartProductViewholder cartProductViewholder = new CartProductViewholder(view);
        return cartProductViewholder;
    }

    @Override
    public void onBindViewHolder(CartProductViewholder holder, int position) {

        final CartProduct cartProduct = cartProducts_list.get(position);

        Picasso.with(context).load(cartProduct.getProd_img_url()).into(holder.cartProdImg);
        holder.cartProdTitle.setText(cartProduct.getProd_name());
        holder.cartProdPts.setText(cartProduct.getProd_pts() + " PTS");

        holder.cartProdQty.setText(cartProduct.getProd_qty());

        if (cartProduct.getProd_qty().equals("1")) {
            holder.reduceCartItem.setVisibility(View.GONE);
        } else {
            holder.reduceCartItem.setVisibility(View.VISIBLE);
        }

        holder.vHclickHandler = this.clickHandler;
    }

    @Override
    public int getItemCount() {
        return cartProducts_list.size();
    }

    public CartProduct getItem(int position) {
        return cartProducts_list.get(position);
    }

    public interface ClickHandler {
        void onMyButtonClicked(final int position, int qty, int mode);
    }

    //0 for removing cart item, 1 for updating cart ite qty

    class CartProductViewholder extends RecyclerView.ViewHolder {

        final ImageView cartProdImg;
        final TextView cartProdTitle;
        final TextView cartProdPts;
        final TextView cartProdQty;

        ClickHandler vHclickHandler;
        final AppCompatImageButton removeCartItem;
        final AppCompatImageButton reduceCartItem;
        final AppCompatImageButton increaseCartItem;

        public CartProductViewholder(View itemView) {
            super(itemView);

            cartProdImg = (ImageView) itemView.findViewById(R.id.cv_prod_img);
            cartProdTitle = (TextView) itemView.findViewById(R.id.cv_prod_name);
            cartProdQty = (TextView) itemView.findViewById(R.id.cv_item_qty);
            cartProdPts = (TextView) itemView.findViewById(R.id.cv_prod_pts);
            removeCartItem = (AppCompatImageButton) itemView.findViewById(R.id.cart_item_remove);
            reduceCartItem = (AppCompatImageButton) itemView.findViewById(R.id.reduce_item);
            increaseCartItem = (AppCompatImageButton) itemView.findViewById(R.id.add_item);


            removeCartItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (vHclickHandler != null) {
                        vHclickHandler.onMyButtonClicked(getAdapterPosition(), Integer.parseInt(cartProdQty.getText().toString()), 0);
                    }
                }
            });

            reduceCartItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (vHclickHandler != null) {
                        vHclickHandler.onMyButtonClicked(getAdapterPosition(), Integer.parseInt(cartProdQty.getText().toString()), 2);
                    }
                }
            });

            increaseCartItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (vHclickHandler != null) {
                        vHclickHandler.onMyButtonClicked(getAdapterPosition(), Integer.parseInt(cartProdQty.getText().toString()), 1);
                    }
                }
            });
        }
    }
}