package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.DealerDetails;

import java.util.ArrayList;

/**
 * Created by Amar on 10-Mar-17.
 */

public class LeaderBoardAdapter extends RecyclerView.Adapter<LeaderBoardAdapter.LeaderBoardViewHolder> {

    private final ArrayList<DealerDetails> dealer_list;
    private final Context context;

    public LeaderBoardAdapter(ArrayList<DealerDetails> dealer_list, Context context) {
        this.dealer_list = dealer_list;
        this.context = context;
    }

    @Override
    public LeaderBoardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemview_leader_board, parent, false);
        LeaderBoardViewHolder requestViewHolder = new LeaderBoardViewHolder(view, context, dealer_list);
        return requestViewHolder;
    }

    @Override
    public void onBindViewHolder(LeaderBoardViewHolder holder, int position) {

        DealerDetails dealer = dealer_list.get(position);

//        if (dealer.getImg_url() != "") {
//            Picasso.with(context).load(dealer.getImg_url()).into(holder.dealer_profile_img);
//        }

        holder.dealer_name_textView.setText(dealer.getName());
//        holder.dealer_amount_textView.setText("Rs. " + dealer.getAmount());
        holder.dealer_rank_textView.setText(dealer.getRank() + "");
    }

    @Override
    public int getItemCount() {
        return dealer_list.size();
    }

    public DealerDetails getItem(int position) {
        return dealer_list.get(position);
    }

    public static class LeaderBoardViewHolder extends RecyclerView.ViewHolder {

        final TextView dealer_name_textView;
        final TextView dealer_amount_textView;
        final TextView dealer_rank_textView;
//        ImageView dealer_profile_img;

        ArrayList<DealerDetails> dealer_list = new ArrayList<>();
        final Context context;

        public LeaderBoardViewHolder(View itemView, Context context, ArrayList<DealerDetails> dealer_list) {
            super(itemView);
            this.dealer_list = dealer_list;
            this.context = context;

            dealer_name_textView = (TextView) itemView.findViewById(R.id.dealer_name);
            dealer_amount_textView = (TextView) itemView.findViewById(R.id.dealer_amount);
            dealer_rank_textView = (TextView) itemView.findViewById(R.id.dealer_rank);
//            dealer_profile_img = (ImageView) itemView.findViewById(R.id.profile_img);
        }
    }
}