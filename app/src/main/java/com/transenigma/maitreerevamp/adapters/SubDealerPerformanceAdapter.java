package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.activities.SubDealerPerformanceDetailsActivity;
import com.transenigma.maitreerevamp.models.SubDealerPerformance;

import java.util.ArrayList;

/**
 * Created by subhadip on 6/4/17.
 */

public class SubDealerPerformanceAdapter extends RecyclerView.Adapter<SubDealerPerformanceAdapter.SubDealerPerformanceAdapterViewHolder> {

    private ArrayList<SubDealerPerformance> sub_dealer_performance_list = new ArrayList<>();
    private final Context context;

    public SubDealerPerformanceAdapter(ArrayList<SubDealerPerformance> sub_dealer_performance_list, Context context) {
        this.sub_dealer_performance_list = sub_dealer_performance_list;
        this.context = context;
    }

    @Override
    public SubDealerPerformanceAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_sub_dealer_perf, parent,
                false);
        SubDealerPerformanceAdapterViewHolder subDealerPerformanceAdapterViewHolder = new
                SubDealerPerformanceAdapterViewHolder(view, context, sub_dealer_performance_list);

        return subDealerPerformanceAdapterViewHolder;
    }

    @Override
    public void onBindViewHolder(SubDealerPerformanceAdapterViewHolder holder, int position) {

        SubDealerPerformance subDealerPerformance = sub_dealer_performance_list.get(position);
        holder.subDealerName.setText(subDealerPerformance.getSubdealer_name());
        holder.subDealerCode.setText(subDealerPerformance.getSubdealer_code());
        holder.subDealerTotalSale.setText(subDealerPerformance.getTotal_sale_no());
        holder.subDealerStatus.setText(subDealerPerformance.getStatus());
    }

    @Override
    public int getItemCount() {
        return sub_dealer_performance_list.size();
    }

    public static class SubDealerPerformanceAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        final ArrayList<SubDealerPerformance> subDealerPerformances_list;
        final TextView subDealerName;
        final TextView subDealerCode;
        final TextView subDealerTotalSale;
        final TextView subDealerStatus;
        final Context context;


        public SubDealerPerformanceAdapterViewHolder(View itemView, Context context, ArrayList<SubDealerPerformance>
                subDealerPerformances_list) {
            super(itemView);
            this.context = context;
            this.subDealerPerformances_list = subDealerPerformances_list;
            subDealerName = (TextView) itemView.findViewById(R.id.sub_dealer_perf_name_tv);
            subDealerCode = (TextView) itemView.findViewById(R.id.sub_dealer_code_tv);
            subDealerTotalSale = (TextView) itemView.findViewById(R.id.sub_dealer_total_sale_tv);
            subDealerStatus = (TextView) itemView.findViewById(R.id.sub_dealer_status_tv);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            SubDealerPerformance subDealerPerformance = subDealerPerformances_list.get(position);

            Intent sub_dealer_perf_details_intent = new Intent(context, SubDealerPerformanceDetailsActivity.class);
            sub_dealer_perf_details_intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            sub_dealer_perf_details_intent.putExtra("child_contact_id", subDealerPerformance.getChild_id());
            sub_dealer_perf_details_intent.putExtra("child_hierarchies_id", subDealerPerformance.getChild_hier_id());
            sub_dealer_perf_details_intent.putExtra("child_is_verified", subDealerPerformance.getChils_is_verified());

            context.startActivity(sub_dealer_perf_details_intent);
        }
    }
}
