package com.transenigma.maitreerevamp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by subhadip on 4/12/16.
 */

public class ProductDetailsPagerAdapter extends FragmentPagerAdapter {

    private final ArrayList<Fragment> fragments = new ArrayList<>();
    private final String[] tabTitles = new String[]{"IMAGES", "DESCRIPTION", "SPECS"};

    public ProductDetailsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragments(Fragment fragments) {
        this.fragments.add(fragments);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
