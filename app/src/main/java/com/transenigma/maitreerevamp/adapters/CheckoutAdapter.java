package com.transenigma.maitreerevamp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.transenigma.maitreerevamp.R;
import com.transenigma.maitreerevamp.models.CheckoutProduct;

import java.util.ArrayList;

/**
 * Created by Amar on 10-Mar-17.
 */


public class CheckoutAdapter extends RecyclerView.Adapter<CheckoutAdapter.CheckoutViewHolder> {

    private final ArrayList<CheckoutProduct> product_list;
    private final Context context;

    public CheckoutAdapter(ArrayList<CheckoutProduct> product_list, Context context) {

        this.product_list = product_list;
        this.context = context;
    }

    @Override
    public CheckoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_checkout,
                parent, false);
        CheckoutViewHolder requestViewHolder = new CheckoutViewHolder(view, context, product_list);
        return requestViewHolder;
    }

    @Override
    public void onBindViewHolder(CheckoutViewHolder holder, int position) {
        CheckoutProduct product = product_list.get(position);

        if (!product.getCheckout_product_img().equals("")) {
            Picasso.with(context).load(product.getCheckout_product_img()).into(holder
                    .checkoutProdImg);
        }

        holder.product_name_textview.setText(product.getCheckout_product_title());
        holder.product_points.setText(product.getCheckout_product_qty() + " x " + product.getCheckout_product_pts());

        int total = Integer.parseInt(product.getCheckout_product_qty()) * Integer.parseInt
                (product.getCheckout_product_pts());

        holder.product_points.append(" = " + total + " PTS");
    }

    @Override
    public int getItemCount() {
        return product_list.size();
    }

    public CheckoutProduct getItem(int position) {
        return product_list.get(position);
    }

    public static class CheckoutViewHolder extends RecyclerView.ViewHolder {

        final TextView product_name_textview;
        final TextView product_points;
        final ImageView checkoutProdImg;
        ArrayList<CheckoutProduct> product_list = new ArrayList<>();
        final Context context;


        public CheckoutViewHolder(View itemView, Context context, ArrayList<CheckoutProduct>
                product_list) {

            super(itemView);
            this.product_list = product_list;
            this.context = context;
            checkoutProdImg = (ImageView) itemView.findViewById(R.id.cv_checkout_prod_img);
            product_name_textview = (TextView) itemView.findViewById(R.id.checkout_prod_title);
            product_points = (TextView) itemView.findViewById(R.id.checkout_prod_pts);
        }
    }
}
